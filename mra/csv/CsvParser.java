package mra.csv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Stack;
import mra.dimension.Matrix;

public class CsvParser {

	public static Matrix<String> parse(File csvFile) throws IOException {
		return CsvParser.parse(csvFile, ",", '"');
	}

	public static Matrix<String> parse(File csvFile, String delimiterString) throws IOException {
		return CsvParser.parse(csvFile, delimiterString, '"');
	}

	public static Matrix<String> parse(File csvFile, char quote) throws IOException {
		return CsvParser.parse(csvFile, ",", quote);
	}

	public static Matrix<String> parse(File csvFile, String delimiterString, char quote) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(csvFile));
		StringBuilder sb = new StringBuilder();
		for (String line; (line = br.readLine()) != null;) {
			sb.append("\n");
			sb.append(line);
		}
		sb.delete(0, 1);
		return CsvParser.parse(sb.toString(), delimiterString, quote);
	}

	public static Matrix<String> parse(String csv) {
		return CsvParser.parse(csv, ",", '"');
	}

	public static Matrix<String> parse(String csv, String delimiterString) {
		return CsvParser.parse(csv, delimiterString, '"');
	}

	public static Matrix<String> parse(String csv, char quote) {
		return CsvParser.parse(csv, ",", quote);
	}

	public static Matrix<String> parse(String csv, String delimiterString, char quote) {
		int columns = 0;
		boolean canOpenString = true;
		boolean openString = false;
		boolean openQuote = false;
		char[] delimiterChars = delimiterString.toCharArray();
		StringBuilder tempString = new StringBuilder();
		Stack<Stack<String>> stringss = new Stack<>();
		stringss.add(new Stack<String>());
		for (char c : csv.toCharArray()) {
			if (c == quote) {
				if (canOpenString) {
					if (openString) {
						if (openQuote) {
							tempString.append(quote);
							openQuote = false;
						} else {
							openQuote = true;
						}
					} else {
						openString = true;
					}
				} else {
					tempString.append(quote);
				}
			} else if (c == '\n') {
				if (openString) {
					if (openQuote) {
						stringss.peek().add(tempString.toString());
						tempString.delete(0, tempString.length());
						columns = Math.max(columns, stringss.peek().size());
						stringss.add(new Stack<String>());
						openQuote = false;
						openString = false;
						canOpenString = true;
					} else {
						tempString.append(c);
					}
				} else {
					stringss.peek().add(tempString.toString());
					tempString.delete(0, tempString.length());
					stringss.add(new Stack<String>());
					canOpenString = true;
				}
			} else {
				boolean isDelimiter = false;
				for (char dc : delimiterChars) {
					if (c == dc) {
						isDelimiter = true;
						break;
					}
				}
				if (isDelimiter) {
					if (openString) {
						if (openQuote) {
							stringss.peek().add(tempString.toString());
							tempString.delete(0, tempString.length());
							openQuote = false;
							openString = false;
							canOpenString = true;
						} else {
							tempString.append(c);
						}
					} else {
						stringss.peek().add(tempString.toString());
						tempString.delete(0, tempString.length());
						canOpenString = true;
					}
				} else {
					if (openString) {
						if (openQuote) {
							tempString.append(quote);
							tempString.append(c);
							openQuote = false;
						} else {
							tempString.append(c);
						}
					} else {
						tempString.append(c);
						canOpenString = false;
					}
				}
			}
		}
		stringss.peek().add(tempString.toString());
		tempString.delete(0, tempString.length());
		columns = Math.max(columns, stringss.peek().size());
		String[][] $stringss = new String[stringss.size()][columns];
		int i = 0;
		for (Stack<String> strings : stringss) {
			int j = 0;
			for (String string : strings) {
				$stringss[i][j] = string;
				j++;
			}
			i++;
		}
		return new Matrix<>($stringss);
	}

	private CsvParser() {
	}
}
