package mra.image;

import java.awt.image.BufferedImage;

public interface Processor {

	public BufferedImage process(BufferedImage image);
}
