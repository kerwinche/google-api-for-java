package mra.image;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class Rotate implements Processor {

	private final double degrees;
	private final Color color;

	public Rotate(double degrees, Color color) {
		this.degrees = degrees % 360.0 + 360.0 % 360.0;
		this.color = color;
	}

	@Override
	public BufferedImage process(BufferedImage image) {
		int oldWidth = image.getWidth();
		int oldHeight = image.getHeight();
		double sin = Math.abs(Math.sin(Math.toRadians(this.degrees)));
		double cos = Math.abs(Math.cos(Math.toRadians(this.degrees)));
		int newWidth = new Double(Math.floor(oldWidth * cos + oldHeight * sin)).intValue();
		int newHeight = new Double(Math.floor(oldHeight * cos + oldWidth * sin)).intValue();
		BufferedImage image2 = new BufferedImage(newWidth, newHeight, image.getType());
		Graphics2D g2d = image2.createGraphics();
		Color color = g2d.getColor();
		g2d.setColor(this.color);
		g2d.fillRect(0, 0, newWidth, newHeight);
		g2d.setColor(color);
		g2d.translate((newWidth - oldWidth) / 2, (newHeight - oldHeight) / 2);
		g2d.rotate(Math.toRadians(this.degrees), oldWidth / 2, oldHeight / 2);
		g2d.drawRenderedImage(image, null);
		g2d.dispose();
		return image2;
	}
}
