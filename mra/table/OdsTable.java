package mra.table;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class OdsTable extends MultiStyledTable {

	private static final Map<Integer, String> HORIZONTAL_ALIGNMENTS = new LinkedHashMap<>();
	private static final Map<Integer, String> VERTICAL_ALIGNMENTS = new LinkedHashMap<>();
	private static final Map<Integer, String> BORDER_STYLES = new LinkedHashMap<>();
	private static final String MIME_TYPE = "application/vnd.oasis.opendocument.spreadsheet";
	private static final String EXTENSION = "ods";
	private static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";

	static {
		OdsTable.HORIZONTAL_ALIGNMENTS.put(0, "left");
		OdsTable.HORIZONTAL_ALIGNMENTS.put(1, "center");
		OdsTable.HORIZONTAL_ALIGNMENTS.put(2, "right");
		OdsTable.VERTICAL_ALIGNMENTS.put(0, "top");
		OdsTable.VERTICAL_ALIGNMENTS.put(1, "middle");
		OdsTable.VERTICAL_ALIGNMENTS.put(2, "bottom");
		OdsTable.BORDER_STYLES.put(0, "solid");
		OdsTable.BORDER_STYLES.put(1, "dashed");
		OdsTable.BORDER_STYLES.put(2, "dotted");
	}

	public void export(File file) throws IOException {
		ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(file));
		zos.putNextEntry(new ZipEntry("META-INF/manifest.xml"));
		this.writeString(zos, OdsTable.XML_HEADER);
		this.writeString(zos, "\n");
		this.writeString(zos, "<manifest:manifest xmlns:manifest=\"urn:oasis:names:tc:opendocument:xmlns:manifest:1.0\">\n");
		this.writeString(zos, String.format("\t<manifest:file-entry manifest:media-type=\"%s\" manifest:full-path=\"/\"/>\n", OdsTable.MIME_TYPE));
		this.writeString(zos, "\t<manifest:file-entry manifest:media-type=\"text/xml\" manifest:full-path=\"content.xml\"/>\n");
		this.writeString(zos, "</manifest:manifest>");
		zos.closeEntry();
		zos.putNextEntry(new ZipEntry("content.xml"));
		this.writeString(zos, OdsTable.XML_HEADER);
		this.writeString(zos, "\n");
		this.writeString(zos, "<office:document-content xmlns:office=\"urn:oasis:names:tc:opendocument:xmlns:office:1.0\" xmlns:style=\"urn:oasis:names:tc:opendocument:xmlns:style:1.0\" xmlns:text=\"urn:oasis:names:tc:opendocument:xmlns:text:1.0\" xmlns:table=\"urn:oasis:names:tc:opendocument:xmlns:table:1.0\" xmlns:fo=\"urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0\" office:version=\"1.2\">\n");
		this.writeString(zos, "\t<office:automatic-styles>\n");
		for (Integer id : this.tableRowStyles.keySet()) {
			TableRowStyle tableRowStyle = this.tableRowStyles.get(id);
			if (tableRowStyle != null) {
				String unit = tableRowStyle.getUnit();
				if (unit == null) {
					unit = "";
				}
				this.writeString(zos, String.format("\t\t<style:style style:name=\"ro-%08x\" style:family=\"table-row\">\n", id));
				this.writeString(zos, "\t\t\t<style:table-row-properties fo:break-before=\"auto\" style:use-optimal-row-height=\"false\"");
				this.writeString(zos, String.format(" style:row-height=\"%.2f%s", tableRowStyle.getHeight(), unit));
				this.writeString(zos, "\"");
				this.writeString(zos, "/>\n");
				this.writeString(zos, "\t\t</style:style>\n");
			}
		}
		for (Integer id : this.tableColumnStyles.keySet()) {
			TableColumnStyle tableColumnStyle = this.tableColumnStyles.get(id);
			if (tableColumnStyle != null) {
				String unit = tableColumnStyle.getUnit();
				if (unit == null) {
					unit = "";
				}
				this.writeString(zos, String.format("\t\t<style:style style:name=\"co-%08x\" style:family=\"table-column\">\n", id));
				this.writeString(zos, "\t\t\t<style:table-column-properties fo:break-before=\"auto\" style:use-optimal-column-width=\"false\"");
				this.writeString(zos, String.format(" style:column-width=\"%.2f%s", tableColumnStyle.getWidth(), unit));
				this.writeString(zos, "\"");
				this.writeString(zos, "/>\n");
				this.writeString(zos, "\t\t</style:style>\n");
			}
		}
		for (Integer id : this.tableCellStyles.keySet()) {
			TableCellStyle tableCellStyle = this.tableCellStyles.get(id);
			if (tableCellStyle != null) {
				this.writeString(zos, String.format("\t\t<style:style style:name=\"ce-%08x\" style:family=\"table-cell\">\n", id));
				int horizontalAlignment = tableCellStyle.getHorizontalAlignment();
				int verticalAlignment = tableCellStyle.getVerticalAlignment();
				Color textColor = tableCellStyle.getTextColor();
				Color backgroundColor = tableCellStyle.getBackgroundColor();
				Font font = tableCellStyle.getFont();
				Map<String, TableBorderStyle> tableBorderStyles = new LinkedHashMap<>();
				tableBorderStyles.put("top", tableCellStyle.getBorderTop());
				tableBorderStyles.put("left", tableCellStyle.getBorderLeft());
				tableBorderStyles.put("right", tableCellStyle.getBorderRight());
				tableBorderStyles.put("bottom", tableCellStyle.getBorderBottom());
				switch (horizontalAlignment) {
					case TableCellStyle.HORIZONTAL_ALIGNMENT_CENTER:
					case TableCellStyle.HORIZONTAL_ALIGNMENT_RIGHT: {
					}
					break;
					default: {
						horizontalAlignment = TableCellStyle.HORIZONTAL_ALIGNMENT_LEFT;
					}
				}
				switch (verticalAlignment) {
					case TableCellStyle.VERTICAL_ALIGNMENT_MIDDLE:
					case TableCellStyle.VERTICAL_ALIGNMENT_BOTTOM: {
					}
					break;
					default: {
						verticalAlignment = TableCellStyle.VERTICAL_ALIGNMENT_TOP;
					}
				}
				this.writeString(zos, "\t\t\t<style:table-cell-properties style:text-align-source=\"fix\" style:repeat-content=\"false\"");
				this.writeString(zos, String.format(" style:vertical-align=\"%s\"", OdsTable.VERTICAL_ALIGNMENTS.get(verticalAlignment)));
				if (backgroundColor != null) {
					this.writeString(zos, String.format(" fo:background-color=\"#%06x\"", backgroundColor.getRGB() & 0xffffff));
				}
				for (String position : tableBorderStyles.keySet()) {
					TableBorderStyle tableBorderStyle = tableBorderStyles.get(position);
					int style = tableBorderStyle.getStyle();
					String unit = tableBorderStyle.getUnit();
					switch (style) {
						case TableBorderStyle.DASHED:
						case TableBorderStyle.DOTTED: {
						}
						break;
						default: {
							style = TableBorderStyle.SOLID;
						}
					}
					if (unit == null) {
						unit = "";
					}
					Color color = tableBorderStyle.getColor();
					if (color == null) {
						color = Color.BLACK;
					}
					this.writeString(zos, String.format(" fo:border-%s=\"%.2f%s %s #%06x\"", position, tableBorderStyle.getThick(), unit, OdsTable.BORDER_STYLES.get(style), color.getRGB() & 0xffffff));
				}
				this.writeString(zos, "/>\n");
				this.writeString(zos, "\t\t\t<style:text-properties");
				if (textColor != null) {
					this.writeString(zos, String.format(" fo:color=\"#%06x\"", textColor.getRGB() & 0xffffff));
				}
				if (font != null) {
					String unit = font.getUnit();
					if (unit == null) {
						unit = "";
					}
					this.writeString(zos, String.format(" fo:font-size=\"%1$.2f%2$s\" style:font-size-asian=\"%1$.2f%2$s\" style:font-size-complex=\"%1$.2f%2$s\"", font.getSize(), unit));
					int style = font.getStyle();
					if ((style & Font.BOLD) > 0) {
						this.writeString(zos, " fo:font-weight=\"bold\" style:font-weight-asian=\"bold\" style:font-weight-complex=\"bold\"");
					}
					if ((style & Font.ITALIC) > 0) {
						this.writeString(zos, " fo:font-style=\"italic\" style:font-style-asian=\"italic\" style:font-style-complex=\"italic\"");
					}
					String name = font.getName();
					if (name != null) {
						this.writeString(zos, String.format(" style:font-name=\"%s\"", name));
					}
				}
				this.writeString(zos, "/>\n");
				this.writeString(zos, String.format("\t\t\t<style:paragraph-properties fo:text-align=\"%s\"/>\n", OdsTable.HORIZONTAL_ALIGNMENTS.get(horizontalAlignment)));
				this.writeString(zos, "\t\t</style:style>\n");
			}
		}
		this.writeString(zos, "\t</office:automatic-styles>\n");
		this.writeString(zos, "\t<office:body>\n");
		this.writeString(zos, "\t\t<office:spreadsheet>\n");
		for (String name : this.tableData.keySet()) {
			this.writeString(zos, String.format("\t\t\t<table:table table:name=\"%s\">\n", name));
			int rows = this.getRowCount(name);
			int columns = this.getColumnCount(name);
			for (int column = 0; column < columns; column++) {
				this.writeString(zos, "\t\t\t\t<table:table-column");
				Integer id = this.tableColumnStyleIds.get(name).get(column);
				if (id != null) {
					this.writeString(zos, String.format(" table:style-name=\"co-%08x\"", id));
				}
				this.writeString(zos, "/>\n");
			}
			for (int row = 0; row < rows; row++) {
				this.writeString(zos, "\t\t\t\t<table:table-row");
				Integer id = this.tableRowStyleIds.get(name).get(row);
				if (id != null) {
					this.writeString(zos, String.format(" table:style-name=\"ro-%08x\"", id));
				}
				this.writeString(zos, ">\n");
				for (int column = 0; column < columns; column++) {
					this.writeString(zos, "\t\t\t\t\t<table:table-cell office:value-type=\"string\"");
					id = this.tableCellStyleIds.get(name).get(row, column);
					if (id != null) {
						this.writeString(zos, String.format(" table:style-name=\"ce-%08x\"", id));
					}
					TableCellStyle tableCellStyle = this.tableCellStyles.get(id);
					if (tableCellStyle != null) {
						int rowspan = tableCellStyle.getRowspan();
						int colspan = tableCellStyle.getColspan();
						if (rowspan > 1) {
							this.writeString(zos, String.format(" table:number-rows-spanned=\"%d\"", rowspan));
						}
						if (colspan > 1) {
							this.writeString(zos, String.format(" table:number-columns-spanned=\"%d\"", colspan));
						}
					}
					this.writeString(zos, ">\n");
					this.writeString(zos, "\t\t\t\t\t\t<text:p>");
					String data = this.tableData.get(name).get(row, column);
					if (data != null) {
						this.writeString(zos, data);
					}
					this.writeString(zos, "</text:p>\n");
					this.writeString(zos, "\t\t\t\t\t</table:table-cell>\n");
				}
				this.writeString(zos, "\t\t\t\t</table:table-row>\n");
			}
			this.writeString(zos, "\t\t\t</table:table>\n");
		}
		this.writeString(zos, "\t\t</office:spreadsheet>\n");
		this.writeString(zos, "\t</office:body>\n");
		this.writeString(zos, "</office:document-content>");
		zos.closeEntry();
		zos.flush();
		zos.close();
	}

	private void writeString(OutputStream os, String string) throws IOException {
		os.write(string.getBytes("UTF-8"));
	}
}
