package mra.table;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import mra.dimension.Array;
import mra.dimension.Matrix;

public abstract class MultiStyledTable {

	protected final Map<Integer, TableRowStyle> tableRowStyles = new LinkedHashMap<>();
	protected final Map<Integer, TableColumnStyle> tableColumnStyles = new LinkedHashMap<>();
	protected final Map<Integer, TableCellStyle> tableCellStyles = new LinkedHashMap<>();
	protected final Map<String, Array<Integer>> tableRowStyleIds = new LinkedHashMap<>();
	protected final Map<String, Array<Integer>> tableColumnStyleIds = new LinkedHashMap<>();
	protected final Map<String, Matrix<Integer>> tableCellStyleIds = new LinkedHashMap<>();
	protected final Map<String, Matrix<String>> tableData = new LinkedHashMap<>();
	protected final Map<String, Matrix<Boolean>> tableCellSkip = new LinkedHashMap<>();

	public MultiStyledTable() {
	}

	public String[] getTableNames() {
		Set<String> names = this.tableData.keySet();
		return names.toArray(new String[names.size()]);
	}

	public int getRowCount(String name) {
		return this.tableData.get(name).getRowCount();
	}

	public int getColumnCount(String name) {
		return this.tableData.get(name).getRowCount();
	}

	private void addTable(String name) {
		if (!this.tableData.containsKey(name)) {
			this.tableRowStyleIds.put(name, new Array<Integer>());
			this.tableColumnStyleIds.put(name, new Array<Integer>());
			this.tableCellStyleIds.put(name, new Matrix<Integer>());
			this.tableData.put(name, new Matrix<String>());
			this.tableCellSkip.put(name, new Matrix<Boolean>());
		}
	}

	public void setData(String name, int row, int column, String data) {
		this.addTable(name);
		int rows = row + 1;
		int columns = column + 1;
		if (rows > this.getRowCount(name) || columns > this.getRowCount(name)) {
			this.tableRowStyleIds.get(name).resize(rows, true);
			this.tableColumnStyleIds.get(name).resize(columns, true);
			this.tableCellStyleIds.get(name).resize(rows, columns, true);
			this.tableCellSkip.get(name).resize(rows, columns, true);
		}
		this.tableData.get(name).set(row, column, data);
	}

	public void setTableRowStyle(String name, int row, TableRowStyle tableRowStyle) {
		this.addTable(name);
		int id = -1;
		if (tableRowStyle != null) {
			id = tableRowStyle.hashCode();
			this.tableRowStyles.put(id, tableRowStyle);
		}
		int rows = row + 1;
		if (rows > this.getRowCount(name)) {
			this.tableCellStyleIds.get(name).resize(rows, -1, true);
			this.tableData.get(name).resize(rows, -1, true);
			this.tableCellSkip.get(name).resize(rows, -1, true);
		}
		if (id > -1) {
			this.tableRowStyleIds.get(name).set(row, id);
		}
	}

	public void setTableColumnStyle(String name, int column, TableColumnStyle tableColumnStyle) {
		this.addTable(name);
		int id = -1;
		if (tableColumnStyle != null) {
			id = tableColumnStyle.hashCode();
			this.tableColumnStyles.put(id, tableColumnStyle);
		}
		int columns = column + 1;
		if (columns > this.getRowCount(name)) {
			this.tableCellStyleIds.get(name).resize(-1, columns, true);
			this.tableData.get(name).resize(-1, columns, true);
			this.tableCellSkip.get(name).resize(-1, columns, true);
		}
		if (id > -1) {
			this.tableColumnStyleIds.get(name).set(column, id);
		}
	}

	public void setTableCellStyle(String name, int row, int column, TableCellStyle tableCellStyle) {
		this.addTable(name);
		int id = -1;
		if (tableCellStyle != null) {
			int rows = this.getRowCount(name);
			int columns = this.getColumnCount(name);
			int rowspan = tableCellStyle.getRowspan();
			int colspan = tableCellStyle.getColspan();
			for (int r = row; r <= row + rowspan - 1; r++) {
				for (int c = column; c <= column + colspan - 1; c++) {
					if (r < rows && c < columns) {
						this.tableCellSkip.get(name).set(r, c, true);
					}
				}
			}
			this.tableCellSkip.get(name).resize(rows, columns, true);
			id = tableCellStyle.hashCode();
			this.tableCellStyles.put(id, tableCellStyle);
		}
		int rows = row + 1;
		int columns = column + 1;
		if (rows > this.getRowCount(name) || columns > this.getRowCount(name)) {
			this.tableRowStyleIds.get(name).set(row, null);
			this.tableColumnStyleIds.get(name).set(column, null);
			this.tableData.get(name).set(rows, column, null);
			this.tableCellSkip.get(name).set(row, column, null);
		}
		if (id > -1) {
			this.tableCellStyleIds.get(name).set(row, column, id);
		}
	}

	public abstract void export(File file) throws IOException;
}
