package mra.table;

import java.awt.Color;

public class TableCellStyle {

	public static final int HORIZONTAL_ALIGNMENT_LEFT = 0;
	public static final int HORIZONTAL_ALIGNMENT_CENTER = 1;
	public static final int HORIZONTAL_ALIGNMENT_RIGHT = 2;
	public static final int VERTICAL_ALIGNMENT_TOP = 0;
	public static final int VERTICAL_ALIGNMENT_MIDDLE = 1;
	public static final int VERTICAL_ALIGNMENT_BOTTOM = 2;
	private final int horizontalAlignment;
	private final int verticalAlignment;
	private final Color textColor;
	private final Color backgroundColor;
	private final Font font;
	private final TableBorderStyle borderTop;
	private final TableBorderStyle borderLeft;
	private final TableBorderStyle borderRight;
	private final TableBorderStyle borderBottom;
	private final int rowspan;
	private final int colspan;

	public TableCellStyle(int horizontalAlignment, int verticalAlignment, Color textColor, Color backgroundColor, Font font, TableBorderStyle borderTop, TableBorderStyle borderLeft, TableBorderStyle borderRight, TableBorderStyle borderBottom, int rowspan, int colspan) {
		this.horizontalAlignment = horizontalAlignment;
		this.verticalAlignment = verticalAlignment;
		this.textColor = textColor;
		this.backgroundColor = backgroundColor;
		this.font = font;
		this.borderTop = borderTop;
		this.borderLeft = borderLeft;
		this.borderRight = borderRight;
		this.borderBottom = borderBottom;
		this.rowspan = rowspan;
		this.colspan = colspan;
	}

	public int getHorizontalAlignment() {
		return this.horizontalAlignment;
	}

	public int getVerticalAlignment() {
		return this.verticalAlignment;
	}

	public Color getTextColor() {
		return this.textColor;
	}

	public Color getBackgroundColor() {
		return this.backgroundColor;
	}

	public Font getFont() {
		return this.font;
	}

	public TableBorderStyle getBorderTop() {
		return this.borderTop;
	}

	public TableBorderStyle getBorderLeft() {
		return this.borderLeft;
	}

	public TableBorderStyle getBorderRight() {
		return this.borderRight;
	}

	public TableBorderStyle getBorderBottom() {
		return this.borderBottom;
	}

	public int getRowspan() {
		return this.rowspan;
	}

	public int getColspan() {
		return this.colspan;
	}
}
