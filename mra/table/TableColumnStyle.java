package mra.table;

public class TableColumnStyle {

	private final float width;
	private final String unit;

	public TableColumnStyle(float width, String unit) {
		this.width = width;
		this.unit = unit;
	}

	public float getWidth() {
		return this.width;
	}

	public String getUnit() {
		return this.unit;
	}
}
