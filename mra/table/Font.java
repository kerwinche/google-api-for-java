package mra.table;

public class Font {

	public static final int BOLD = 1;
	public static final int ITALIC = 2;
	private final String name;
	private final int style;
	private final float size;
	private final String unit;

	public Font(String name, int style, float size, String unit) {
		this.name = name;
		this.style = style;
		this.size = size;
		this.unit = unit;
	}

	public String getName() {
		return this.name;
	}

	public int getStyle() {
		return this.style;
	}

	public float getSize() {
		return this.size;
	}

	public String getUnit() {
		return this.unit;
	}
}
