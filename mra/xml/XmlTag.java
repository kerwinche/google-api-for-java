package mra.xml;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import mra.json.JsonObject;
import mra.json.JsonString;
import mra.json.JsonValue;

public class XmlTag extends XmlObject {

	private final String tagName;
	private final Map<String, String> xmlAttributes;
	private final ArrayList<XmlObject> xmlObjects = new ArrayList<>();

	public XmlTag(String tagName) {
		this(tagName, new LinkedHashMap<String, String>(), new XmlObject[0]);
	}

	public XmlTag(String tagName, Map<String, String> xmlAttributes) {
		this(tagName, xmlAttributes, new XmlObject[0]);
	}

	public XmlTag(String tagName, XmlObject[] xmlObjects) {
		this(tagName, new LinkedHashMap<String, String>(), xmlObjects);
	}

	public XmlTag(String tagName, Map<String, String> xmlAttributes, XmlObject[] xmlObjects) {
		this.tagName = tagName;
		this.xmlAttributes = xmlAttributes;
		for (XmlObject xmlObject : xmlObjects) {
			if (xmlObject != null) {
				this.xmlObjects.add(xmlObject);
			}
		}
	}

	public String getTagName() {
		return this.tagName;
	}

	public void setXmlAttribute(String key, String value) {
		this.xmlAttributes.put(key, value);
	}

	public String getXmlAttribute(String key) {
		return this.xmlAttributes.get(key);
	}

	public Map<String, String> getXmlAttributes() {
		return this.xmlAttributes;
	}

	public void removeXmlAtribute(String key) {
		this.xmlAttributes.remove(key);
	}

	public void clearXmlAttributes() {
		this.xmlAttributes.clear();
	}

	public void addXmlObject(XmlObject xmlObject) {
		this.xmlObjects.add(xmlObject);
	}

	public XmlObject getXmlObject(int index) {
		return this.xmlObjects.get(index);
	}

	public XmlObject[] getXmlObjects() {
		return this.xmlObjects.toArray(new XmlObject[this.xmlObjects.size()]);
	}

	public void removeXmlObject(int index) {
		this.xmlObjects.remove(index);
	}

	public void clearXmlObjects() {
		this.xmlObjects.clear();
	}

	public XmlTag[] getXmlTags(String tagName) {
		ArrayList<XmlTag> xmlTags = new ArrayList<>();
		for (XmlObject xmlObject : this.xmlObjects) {
			if (xmlObject instanceof XmlTag) {
				XmlTag xmlTag = (XmlTag) xmlObject;
				if (xmlTag.getTagName().equals(tagName)) {
					xmlTags.add(xmlTag);
				}
			}
		}
		return xmlTags.toArray(new XmlTag[xmlTags.size()]);
	}

	public String getInnerString() {
		XmlObject[] xmlObjects = getXmlObjects();
		if (xmlObjects.length > 0) {
			String string = this.toString();
			int start = string.indexOf('>');
			int end = string.lastIndexOf('<');
			return string.substring(start + 1, end);
		} else {
			return "";
		}
	}

	@Override
	String toString(boolean formatted, int tabs, boolean isFirst) {
		StringBuilder sb = new StringBuilder();
		if (!isFirst && formatted) {
			sb.append("\n");
			for (int j = 0; j < tabs; j++) {
				sb.append("\t");
			}
		}
		sb.append("<");
		sb.append(this.getTagName());
		for (String key : this.xmlAttributes.keySet()) {
			String value = this.xmlAttributes.get(key);
			sb.append(" ");
			sb.append(key);
			sb.append("=\"");
			sb.append(value);
			sb.append('"');
		}
		if (this.xmlObjects.size() > 0) {
			sb.append(">");
			for (XmlObject xmlObject : this.xmlObjects) {
				if (xmlObject instanceof XmlTag) {
					XmlTag xmlTag = (XmlTag) xmlObject;
					sb.append(xmlTag.toString(formatted, tabs + 1, false));
				} else {
					sb.append(xmlObject.toString());
					formatted = false;
				}
			}
			if (formatted) {
				sb.append("\n");
				for (int j = 0; j < tabs; j++) {
					sb.append("\t");
				}
			}
			sb.append("</");
			sb.append(this.getTagName());
		} else {
			sb.append("/");
		}
		sb.append(">");
		return sb.toString();
	}

	@Override
	public JsonValue toJsonValue() {
		JsonObject jsonObject = new JsonObject();
		jsonObject.setValue(this.getTagName(), this.toJsonObject(this, new JsonObject()));
		return jsonObject;
	}

	private JsonObject toJsonObject(XmlTag xmlTag, JsonObject jsonObject) {
		for (String key : this.xmlAttributes.keySet()) {
			String value = this.xmlAttributes.get(key);
			jsonObject.setValue(key, new JsonString(value));
		}
		for (XmlObject xmlObject : xmlTag.xmlObjects) {
			if (xmlObject instanceof XmlTag) {
				XmlTag $XmlTag = (XmlTag) xmlObject;
				jsonObject.setValue($XmlTag.getTagName(), this.toJsonObject($XmlTag, new JsonObject()));
			} else {
				jsonObject.setValue("_", new JsonString(xmlObject.toString()));
			}
		}
		return jsonObject;
	}
}
