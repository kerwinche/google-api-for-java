package mra.xml;

import mra.json.JsonString;
import mra.json.JsonValue;

public class XmlString extends XmlObject {

	private final String string;

	public XmlString(String string) {
		this.string = string;
	}

	public String getString() {
		return this.string;
	}

	@Override
	String toString(boolean formatted, int tabs, boolean isFirst) {
		StringBuilder sb = new StringBuilder();
		if (!isFirst && formatted) {
			sb.append("\n");
			for (int j = 0; j < tabs; j++) {
				sb.append("\t");
			}
		}
		sb.append(this.getString());
		return sb.toString();
	}

	@Override
	public JsonValue toJsonValue() {
		return new JsonString(this.getString());
	}
}
