package mra.google.contact;

public class ContactEmail {

	private final boolean isPrimary;
	private final String type;
	private final String email;

	public ContactEmail(boolean isPrimary, String type, String email) {
		this.isPrimary = isPrimary;
		this.type = type;
		this.email = email;
	}

	public boolean isPrimary() {
		return this.isPrimary;
	}

	public String getType() {
		return this.type;
	}

	public String getEmail() {
		return this.email;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (this.isPrimary()) {
			sb.append("*");
		}
		sb.append("[");
		sb.append(this.getType());
		sb.append("]");
		sb.append(this.getEmail());
		return sb.toString();
	}
}
