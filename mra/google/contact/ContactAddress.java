package mra.google.contact;

public class ContactAddress {

	private final boolean isPrimary;
	private final String type;
	private final String formattedAddress;
	private final String street;
	private final String city;
	private final String region;
	private final String postcode;
	private final String country;

	public ContactAddress(boolean isPrimary, String type, String street, String city, String region, String postcode, String country) {
		this(isPrimary, type, null, street, city, region, postcode, country);
	}

	ContactAddress(boolean isPrimary, String type, String formattedAddress, String street, String city, String region, String postcode, String country) {
		this.isPrimary = isPrimary;
		this.type = type;
		this.formattedAddress = formattedAddress;
		this.street = street;
		this.city = city;
		this.region = region;
		this.postcode = postcode;
		this.country = country;
	}

	public boolean isPrimary() {
		return this.isPrimary;
	}

	public String getType() {
		return this.type;
	}

	public String getFormattedAddress() {
		return this.formattedAddress;
	}

	public String getStreet() {
		return this.street;
	}

	public String getCity() {
		return this.city;
	}

	public String getRegion() {
		return this.region;
	}

	public String getPostcode() {
		return this.postcode;
	}

	public String getCountry() {
		return this.country;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (this.isPrimary()) {
			sb.append("*");
		}
		sb.append("[");
		sb.append(this.getType());
		sb.append("]");
		sb.append(this.getFormattedAddress());
		return sb.toString();
	}
}
