package mra.google.contact;

import java.util.Comparator;

public class ContactGroupComparator implements Comparator<ContactGroup> {

	public static final char SORT_BY_TITLE = 'T';
	public static final char ORDER_BY_ASCENDING = 'A';
	public static final char ORDER_BY_DESCENDING = 'D';
	private char sortBy;
	private char orderBy;

	public ContactGroupComparator(char sortBy, char orderBy) {
		switch (sortBy) {
			default: {
				this.sortBy = ContactGroupComparator.SORT_BY_TITLE;
			}
		}
		switch (orderBy) {
			case ContactEntryComparator.ORDER_BY_DESCENDING: {
				this.orderBy = orderBy;
			}
			break;
			default: {
				this.orderBy = ContactEntryComparator.ORDER_BY_ASCENDING;
			}
		}
	}

	public ContactGroupComparator() {
		this(ContactGroupComparator.SORT_BY_TITLE, ContactGroupComparator.ORDER_BY_ASCENDING);
	}

	@Override
	public int compare(ContactGroup t1, ContactGroup t2) {
		switch (this.sortBy) {
			default: {
				switch (this.orderBy) {
					case ContactEntryComparator.ORDER_BY_DESCENDING: {
						return t2.getTitle().compareTo(t1.getTitle());
					}
					default: {
						return t1.getTitle().compareTo(t2.getTitle());
					}
				}
			}
		}
	}
}
