package mra.google.contact;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import mra.google.GoogleAuthorization;
import mra.http.HttpRequest;
import mra.xml.XmlObject;
import mra.xml.XmlParser;
import mra.xml.XmlTag;

public class ContactBook {

	static Map<String, String> createContactGoogleAuthorizationHeaders(String contentType, GoogleAuthorization googleAuthorization) {
		Map<String, String> requestHeaders = new LinkedHashMap<>();
		requestHeaders.put("Content-Type", contentType + "; charset=" + HttpRequest.CHARSET_UTF8);
		requestHeaders.put("GData-Version", "3");
		if (googleAuthorization != null) {
			requestHeaders.put("Authorization", googleAuthorization.getToken());
		}
		return requestHeaders;
	}

	private GoogleAuthorization googleAuthorization;
	private String user;
	private String userEmail;

	GoogleAuthorization getGoogleAuthorization() {
		return this.googleAuthorization;
	}

	public String getUser() {
		return this.user;
	}

	public String getUserEmail() {
		return this.userEmail;
	}

	public ContactBook(String userEmail, GoogleAuthorization googleAuthorization) throws IOException {
		this.googleAuthorization = googleAuthorization;
		if (!userEmail.contains("@")) {
			userEmail += "@gmail.com";
		}
		URL url = new URL("https://www.google.com/m8/feeds/contacts/" + userEmail + "/full?max-results=0");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(ContactBook.createContactGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		XmlTag xmlTag = XmlParser.parse(httpRequest.getResponseString());
		try {
			this.user = xmlTag.getXmlTags("author")[0].getXmlTags("name")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.userEmail = xmlTag.getXmlTags("author")[0].getXmlTags("email")[0].getInnerString();
		} catch (Exception ex) {
		}
	}

	public ContactEntry getContactEntry(String id) throws IOException {
		GoogleAuthorization googleAuthorization = this.getGoogleAuthorization();
		URL url = new URL("https://www.google.com/m8/feeds/contacts/" + this.getUserEmail() + "/full/" + id);
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(ContactBook.createContactGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		return new ContactEntry(this, xmlTag);
	}

	public ContactEntry[] listContactEntries() throws IOException {
		return this.listContactEntries(ContactEntryComparator.SORT_BY_FULL_NAME, ContactEntryComparator.ORDER_BY_ASCENDING);
	}

	public ContactEntry[] listContactEntries(char sortBy, char orderBy) throws IOException {
		GoogleAuthorization googleAuthorization = this.getGoogleAuthorization();
		URL url = new URL("https://www.google.com/m8/feeds/contacts/" + this.getUserEmail() + "/full?max-results=" + Integer.MAX_VALUE);
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(ContactBook.createContactGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		ArrayList<ContactEntry> contactEntries = new ArrayList<>();
		XmlTag[] entries = xmlTag.getXmlTags("entry");
		for (XmlTag entry : entries) {
			contactEntries.add(new ContactEntry(this, entry));
		}
		ContactEntry[] ces = contactEntries.toArray(new ContactEntry[contactEntries.size()]);
		Arrays.sort(ces, new ContactEntryComparator(sortBy, orderBy));
		return ces;
	}

	public ContactEntry createContactEntry(String namePrefix, String givenName, String additionalName, String familyName, String nameSuffix) throws IOException {
		GoogleAuthorization googleAuthorization = this.getGoogleAuthorization();
		URL url = new URL("https://www.google.com/m8/feeds/contacts/" + this.getUserEmail() + "/full");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		httpRequest.setRequestHeaders(ContactBook.createContactGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		String requestData = ContactEntry.createContactEntryXML(namePrefix, givenName, additionalName, familyName, nameSuffix, null, null, null, null);
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlObject xmlObject = XmlParser.parse(responseString);
		XmlTag xmlTag = ((XmlTag) xmlObject);
		return new ContactEntry(this, xmlTag);
	}

	public ContactGroup getContactGroup(String id) throws IOException {
		GoogleAuthorization googleAuthorization = this.getGoogleAuthorization();
		URL url = new URL("https://www.google.com/m8/feeds/groups/" + this.getUserEmail() + "/full/" + id);
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(ContactBook.createContactGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		return new ContactGroup(this, xmlTag);
	}

	public ContactGroup[] listContactGroups() throws IOException {
		return this.listContactGroups(ContactGroupComparator.SORT_BY_TITLE, ContactGroupComparator.ORDER_BY_ASCENDING);
	}

	public ContactGroup[] listContactGroups(char sortBy, char orderBy) throws IOException {
		GoogleAuthorization googleAuthorization = this.getGoogleAuthorization();
		URL url = new URL("https://www.google.com/m8/feeds/groups/" + this.getUserEmail() + "/full?max-results=" + Integer.MAX_VALUE);
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(ContactBook.createContactGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		ArrayList<ContactGroup> contactGroups = new ArrayList<>();
		XmlTag[] entries = xmlTag.getXmlTags("entry");
		for (XmlTag entry : entries) {
			contactGroups.add(new ContactGroup(this, entry));
		}
		ContactGroup[] cgs = contactGroups.toArray(new ContactGroup[contactGroups.size()]);
		Arrays.sort(cgs, new ContactGroupComparator(sortBy, orderBy));
		return cgs;
	}

	public ContactGroup createContactGroup(String title) throws IOException {
		GoogleAuthorization googleAuthorization = this.getGoogleAuthorization();
		URL url = new URL("https://www.google.com/m8/feeds/groups/" + this.getUserEmail() + "/full");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		httpRequest.setRequestHeaders(ContactBook.createContactGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_ATOM_XML, googleAuthorization));
		String requestData = ContactGroup.createContactGroupXML(title);
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		return new ContactGroup(this, xmlTag);
	}
}
