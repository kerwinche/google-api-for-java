package mra.google;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import mra.http.HttpRequest;
import mra.json.JsonNumber;
import mra.json.JsonObject;
import mra.json.JsonString;

public class GoogleOAuth2 extends GoogleAuthorization {

	public static final String SCOPE_PICASA = "https://picasaweb.google.com/data";
	public static final String SCOPE_YOUTUBE = "https://gdata.youtube.com";
	public static final String SCOPE_DRIVE = "https://www.googleapis.com/auth/drive";
	public static final String SCOPE_SPREADSHEET = "https://spreadsheets.google.com/feeds";
	public static final String SCOPE_APP_ENGINE = "https://www.googleapis.com/auth/appengine.admin";
	public static final String SCOPE_CONTACT = "https://www.google.com/m8/feeds";
	public static final String SCOPE_CALENDAR = "https://www.googleapis.com/auth/calendar";
	private final String redirectURI = "urn:ietf:wg:oauth:2.0:oob";
	private final List<String> scopes = new ArrayList<>();
	private String accessToken;
	private String authToken;
	private String tokenType;
	private int expiresIn;
	private String refreshToken;

	public GoogleOAuth2(String clientId, String clientSecert, String apiKey) {
		super(clientId, clientSecert, apiKey);
	}

	@Override
	public String getToken() {
		return String.format("%s \"%s\"", this.getTokenType(), this.getAuthToken());
	}

	public void addScope(String scope) {
		this.scopes.add(scope);
	}

	public String[] getScopes() {
		return this.scopes.toArray(new String[this.scopes.size()]);
	}

	public String getAccessToken() {
		return this.accessToken;
	}

	public String getAuthToken() {
		return this.authToken;
	}

	public String getTokenType() {
		return this.tokenType;
	}

	public int getExpiresIn() {
		return this.expiresIn;
	}

	public String getRefreshToken() {
		return this.refreshToken;
	}

	public String generateOAuth2Path() {
		StringBuilder scopesString = new StringBuilder();
		for (int i = 0; i < this.scopes.size(); i++) {
			if (i > 0) {
				scopesString.append("+");
			}
			scopesString.append(this.scopes.get(i));
		}
		StringBuilder sb = new StringBuilder();
		sb.append("https://accounts.google.com/o/oauth2/auth");
		sb.append("?response_type=");
		sb.append("code");
		sb.append("&client_id=");
		sb.append(this.getClientId());
		sb.append("&redirect_uri=");
		sb.append(this.redirectURI);
		sb.append("&scope=");
		sb.append(scopesString.toString());
		return sb.toString();
	}

	public void retrieveOAuth2Token(String accessToken) throws IOException {
		this.accessToken = accessToken;
		URL url = new URL("https://www.googleapis.com/oauth2/v3/token");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = new LinkedHashMap<>();
		requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_WWW_FORM + "; charset=" + HttpRequest.CHARSET_UTF8);
		httpRequest.setRequestHeaders(requestHeaders);
		Map<String, String> requestData = new LinkedHashMap();
		requestData.put("code", accessToken);
		requestData.put("client_id", this.getClientId());
		requestData.put("client_secret", this.getClientSecert());
		requestData.put("redirect_uri", this.redirectURI);
		requestData.put("grant_type", "authorization_code");
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		JsonObject jsonObject = JsonObject.parse(httpRequest.getResponseString());
		this.authToken = ((JsonString) jsonObject.getValue("access_token")).getValue();
		this.tokenType = ((JsonString) jsonObject.getValue("token_type")).getValue();
		this.expiresIn = ((JsonNumber) jsonObject.getValue("expires_in")).getValue().intValue();
		this.refreshToken = ((JsonString) jsonObject.getValue("refresh_token")).getValue();
	}

	public void updateOAuth2Token() throws IOException {
		URL url = new URL("https://www.googleapis.com/oauth2/v2/tokeninfo?access_token=" + this.getAuthToken());
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		Map<String, String> requestHeaders = new LinkedHashMap<>();
		requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_WWW_FORM + "; charset=" + HttpRequest.CHARSET_UTF8);
		httpRequest.setRequestHeaders(requestHeaders);
		httpRequest.send();
		JsonObject jsonObject = JsonObject.parse(httpRequest.getResponseString());
		this.expiresIn = ((JsonNumber) jsonObject.getValue("expires_in")).getValue().intValue();
	}

	public void refreshOAuth2Token() throws IOException {
		URL url = new URL("https://www.googleapis.com/oauth2/v3/token");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = new LinkedHashMap<>();
		requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_WWW_FORM + "; charset=" + HttpRequest.CHARSET_UTF8);
		httpRequest.setRequestHeaders(requestHeaders);
		Map<String, String> requestData = new LinkedHashMap();
		requestData.put("client_id", this.getClientId());
		requestData.put("client_secret", this.getClientSecert());
		requestData.put("refresh_token", this.getRefreshToken());
		requestData.put("grant_type", "refresh_token");
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		JsonObject jsonObject = JsonObject.parse(httpRequest.getResponseString());
		this.authToken = ((JsonString) jsonObject.getValue("access_token")).getValue();
		this.tokenType = ((JsonString) jsonObject.getValue("token_type")).getValue();
		this.expiresIn = ((JsonNumber) jsonObject.getValue("expires_in")).getValue().intValue();
	}
}
