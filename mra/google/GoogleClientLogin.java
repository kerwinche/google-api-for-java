package mra.google;

import java.io.IOException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import mra.http.HttpRequest;
import mra.ini.IniObject;
import mra.ini.IniParser;

/**
 * @deprecated
 */
public class GoogleClientLogin extends GoogleAuthorization {

	public static final String SERVICE_PICASA = "lh2";
	public static final String SERVICE_YOUTUBE = "youtube";
	public static final String SERVICE_DRIVE = "writely";
	public static final String SERVICE_APP_ENGINE = "ah";
	public static final String SERVICE_CONTACT = "cp";
	public static final String SERVICE_CALENDAR = "cl";
	private String sid;
	private String lsid;
	private String authToken;

	public GoogleClientLogin(String clientId, String clientSecert, String apiKey, String email, String password, String service) throws IOException {
		this(clientId, clientSecert, apiKey, email, password, service, "Java-Application");
	}

	public GoogleClientLogin(String clientId, String clientSecert, String apiKey, String email, String password, String service, String source) throws IOException {
		super(clientId, clientSecert, apiKey);
		URL url = new URL("https://www.google.com/accounts/ClientLogin");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		Map<String, String> requestHeaders = new LinkedHashMap<>();
		requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_WWW_FORM + "; charset=" + HttpRequest.CHARSET_UTF8);
		httpRequest.setRequestHeaders(requestHeaders);
		Map<String, String> requestData = new LinkedHashMap<>();
		requestData.put("accountType", "HOSTED_OR_GOOGLE");
		requestData.put("Email", email);
		requestData.put("Passwd", password);
		requestData.put("service", service);
		requestData.put("source", source);
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		System.out.println(responseString);
		IniObject iniObject = IniParser.parse(responseString, "");
		this.sid = iniObject.getIniAttribute(null, "SID");
		this.lsid = iniObject.getIniAttribute(null, "LSID");
		this.authToken = iniObject.getIniAttribute(null, "Auth");
	}

	@Override
	public String getToken() {
		return String.format("GoogleLogin auth=\"%s\"", this.getAuthToken());
	}

	public String getAuthToken() {
		return this.authToken;
	}

	public String getSid() {
		return this.sid;
	}

	public String getLsid() {
		return this.lsid;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("SID=");
		sb.append(this.getSid());
		sb.append("\nLSID=");
		sb.append(this.getLsid());
		sb.append("\nAuth=");
		sb.append(this.getAuthToken());
		return sb.toString();
	}
}
