package mra.google.picasa;

import java.util.Comparator;

public class PicasaPhotoComparator implements Comparator<PicasaPhoto> {

	public static final char SORT_BY_TITLE = 'T';
	public static final char SORT_BY_LAST_MODIFIED = 'M';
	public static final char SORT_BY_LENGTH = 'L';
	public static final char ORDER_BY_ASCENDING = 'A';
	public static final char ORDER_BY_DESCENDING = 'D';
	private char sortBy;
	private char orderBy;

	public PicasaPhotoComparator(char sortBy, char orderBy) {
		switch (sortBy) {
			case PicasaPhotoComparator.SORT_BY_LAST_MODIFIED:
			case PicasaPhotoComparator.SORT_BY_LENGTH: {
				this.sortBy = sortBy;
			}
			break;
			default: {
				this.sortBy = PicasaPhotoComparator.SORT_BY_TITLE;
			}
		}
		switch (orderBy) {
			case PicasaPhotoComparator.ORDER_BY_DESCENDING: {
				this.orderBy = orderBy;
			}
			break;
			default: {
				this.orderBy = PicasaPhotoComparator.ORDER_BY_ASCENDING;
			}
		}
	}

	public PicasaPhotoComparator() {
		this(PicasaPhotoComparator.SORT_BY_TITLE, PicasaPhotoComparator.ORDER_BY_ASCENDING);
	}

	@Override
	public int compare(PicasaPhoto t1, PicasaPhoto t2) {
		switch (this.sortBy) {
			case PicasaPhotoComparator.SORT_BY_LAST_MODIFIED: {
				switch (this.orderBy) {
					case PicasaPhotoComparator.ORDER_BY_DESCENDING: {
						return t2.getLastModified().compareTo(t1.getLastModified());
					}
					default: {
						return t1.getLastModified().compareTo(t2.getLastModified());
					}
				}
			}
			case PicasaPhotoComparator.SORT_BY_LENGTH: {
				switch (this.orderBy) {
					case PicasaPhotoComparator.ORDER_BY_DESCENDING: {
						return t2.getLength() - t1.getLength();
					}
					default: {
						return t1.getLength() - t2.getLength();
					}
				}
			}
			default: {
				switch (this.orderBy) {
					case PicasaAlbumComparator.ORDER_BY_DESCENDING: {
						return t2.getTitle().compareTo(t1.getTitle());
					}
					default: {
						return t1.getTitle().compareTo(t2.getTitle());
					}
				}
			}
		}
	}
}
