package mra.google.picasa;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import mra.http.HttpRequest;
import mra.google.Google;
import mra.google.GoogleAuthorization;
import mra.xml.XmlParser;
import mra.xml.XmlTag;

public class PicasaWeb {

	static Map<String, String> createPicasaGoogleAuthorizationHeaders(String contentType, GoogleAuthorization googleAuthorization) {
		Map<String, String> requestHeaders = new LinkedHashMap<>();
		requestHeaders.put("Content-Type", contentType + "; charset=" + HttpRequest.CHARSET_UTF8);
		if (googleAuthorization != null) {
			requestHeaders.put("Authorization", googleAuthorization.getToken());
		}
		return requestHeaders;
	}

	private GoogleAuthorization googleAuthorization;
	private String userId;
	private String author;
	private String description;
	private URL cover;
	private Date lastModified;
	private URL webURL;
	private URL feedURL;
	private URL entryURL;

	GoogleAuthorization getGoogleAuthorization() {
		return this.googleAuthorization;
	}

	public String getUserId() {
		return this.userId;
	}

	public String getAuthor() {
		return this.author;
	}

	public String getDescription() {
		return this.description;
	}

	public URL getCover() {
		return this.cover;
	}

	public Date getLastModified() {
		return this.lastModified;
	}

	public URL getWebURL() {
		return this.webURL;
	}

	URL getFeedURL() {
		return this.feedURL;
	}

	URL getEntryURL() {
		return this.entryURL;
	}

	public PicasaWeb(String userId) throws IOException {
		this(userId, null);
	}

	public PicasaWeb(String userId, GoogleAuthorization googleAuthorization) throws IOException {
		this.googleAuthorization = googleAuthorization;
		URL url = new URL("https://picasaweb.google.com/data/feed/api/user/" + userId + "?max-results=0");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(PicasaWeb.createPicasaGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		try {
			this.userId = xmlTag.getXmlTags("title")[0].getInnerString();
			this.entryURL = new URL("https://picasaweb.google.com/data/entry/api/user/" + this.userId);
		} catch (Exception ex) {
		}
		try {
			this.author = xmlTag.getXmlTags("author")[0].getXmlTags("name")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.description = xmlTag.getXmlTags("subtitle")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.cover = new URL(xmlTag.getXmlTags("icon")[0].getInnerString());
		} catch (Exception ex) {
		}
		try {
			this.lastModified = Google.datetimeStringToDate(xmlTag.getXmlTags("updated")[0].getInnerString());
		} catch (Exception ex) {
		}
		try {
			XmlTag[] links = xmlTag.getXmlTags("link");
			for (XmlTag link : links) {
				String rel = link.getXmlAttribute("rel");
				URL href = new URL(link.getXmlAttribute("href"));
				if (rel.equals("http://schemas.google.com/g/2005#feed")) {
					this.feedURL = href;
				} else if (rel.equals("alternate")) {
					this.webURL = href;
				}
			}
		} catch (Exception ex) {
		}
	}

	public PicasaAlbum createPicasaAlbum(String title, String description, String access, Date lastModified, URL cover) throws IOException {
		GoogleAuthorization googleAuthorization = this.getGoogleAuthorization();
		URL url = this.getFeedURL();
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		httpRequest.setRequestHeaders(PicasaWeb.createPicasaGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_ATOM_XML, googleAuthorization));
		String requestData = PicasaAlbum.createPicasaAlbumXML(title, description, access, lastModified, cover, null);
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		return new PicasaAlbum(this, xmlTag);
	}

	public PicasaAlbum getPicasaAlbum(String albumId) throws IOException {
		GoogleAuthorization googleAuthorization = this.getGoogleAuthorization();
		URL url = new URL(this.getEntryURL().toString() + "/albumid/" + albumId);
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(PicasaWeb.createPicasaGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		return new PicasaAlbum(this, xmlTag);
	}

	public PicasaAlbum[] listPicasaAlbums() throws IOException {
		return this.listPicasaAlbums(PicasaAlbumComparator.SORT_BY_LAST_MODIFIED, PicasaAlbumComparator.ORDER_BY_DESCENDING);
	}

	public PicasaAlbum[] listPicasaAlbums(char sortBy, char orderBy) throws IOException {
		GoogleAuthorization googleAuthorization = this.getGoogleAuthorization();
		URL url = this.getFeedURL();
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(PicasaWeb.createPicasaGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		ArrayList<PicasaAlbum> picasaAlbums = new ArrayList<>();
		XmlTag[] entries = xmlTag.getXmlTags("entry");
		for (XmlTag entry : entries) {
			picasaAlbums.add(new PicasaAlbum(this, entry));
		}
		PicasaAlbum[] pas = picasaAlbums.toArray(new PicasaAlbum[picasaAlbums.size()]);
		Arrays.sort(pas, new PicasaAlbumComparator(sortBy, orderBy));
		return pas;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("User ID = ");
		sb.append(this.getUserId());
		sb.append("\nAuthor = ");
		sb.append(this.getAuthor());
		sb.append("\nDescription = ");
		sb.append(this.getDescription());
		sb.append("\nCover = ");
		sb.append(this.getCover() == null ? "<null>" : this.getCover().toString());
		sb.append("\nLast Modified = ");
		sb.append(this.getLastModified() == null ? "<null>" : Google.toHumanDateTimeFormat(this.getLastModified()));
		sb.append("\nWeb URL = ");
		sb.append(this.getWebURL() == null ? "<null>" : this.getWebURL().toString());
		return sb.toString();
	}
}
