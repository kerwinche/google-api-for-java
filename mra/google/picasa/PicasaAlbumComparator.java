package mra.google.picasa;

import java.util.Comparator;

class PicasaAlbumComparator implements Comparator<PicasaAlbum> {

	public static final char SORT_BY_TITLE = 'T';
	public static final char SORT_BY_PUBLISHED = 'P';
	public static final char SORT_BY_LAST_MODIFIED = 'L';
	public static final char SORT_BY_COUNT = 'C';
	public static final char ORDER_BY_ASCENDING = 'A';
	public static final char ORDER_BY_DESCENDING = 'D';
	private char sortBy;
	private char orderBy;

	public PicasaAlbumComparator(char sortBy, char orderBy) {
		switch (sortBy) {
			case PicasaAlbumComparator.SORT_BY_PUBLISHED:
			case PicasaAlbumComparator.SORT_BY_LAST_MODIFIED:
			case PicasaAlbumComparator.SORT_BY_COUNT: {
				this.sortBy = sortBy;
			}
			break;
			default: {
				this.sortBy = PicasaAlbumComparator.SORT_BY_TITLE;
			}
		}
		switch (orderBy) {
			case PicasaAlbumComparator.ORDER_BY_DESCENDING: {
				this.orderBy = orderBy;
			}
			break;
			default: {
				this.orderBy = PicasaAlbumComparator.ORDER_BY_ASCENDING;
			}
		}
	}

	public PicasaAlbumComparator() {
		this(PicasaAlbumComparator.SORT_BY_LAST_MODIFIED, PicasaAlbumComparator.ORDER_BY_DESCENDING);
	}

	@Override
	public int compare(PicasaAlbum t1, PicasaAlbum t2) {
		switch (this.sortBy) {
			case PicasaAlbumComparator.SORT_BY_PUBLISHED: {
				switch (this.orderBy) {
					case PicasaAlbumComparator.ORDER_BY_DESCENDING: {
						return t2.getPublished().compareTo(t1.getPublished());
					}
					default: {
						return t1.getPublished().compareTo(t2.getPublished());
					}
				}
			}
			case PicasaAlbumComparator.SORT_BY_LAST_MODIFIED: {
				switch (this.orderBy) {
					case PicasaAlbumComparator.ORDER_BY_DESCENDING: {
						return t2.getLastModified().compareTo(t1.getLastModified());
					}
					default: {
						return t1.getLastModified().compareTo(t2.getLastModified());
					}
				}
			}
			case PicasaAlbumComparator.SORT_BY_COUNT: {
				switch (this.orderBy) {
					case PicasaAlbumComparator.ORDER_BY_DESCENDING: {
						return t2.getPhotoCount() - t1.getPhotoCount();
					}
					default: {
						return t1.getPhotoCount() - t2.getPhotoCount();
					}
				}
			}
			default: {
				switch (this.orderBy) {
					case PicasaAlbumComparator.ORDER_BY_DESCENDING: {
						return t2.getTitle().compareTo(t1.getTitle());
					}
					default: {
						return t1.getTitle().compareTo(t2.getTitle());
					}
				}
			}
		}
	}
}
