package mra.google.spreadsheet;

import java.io.IOException;
import java.net.URL;
import mra.google.GoogleAuthorization;
import mra.xml.XmlObject;
import mra.xml.XmlString;
import mra.xml.XmlTag;

public class SpreadsheetWorksheet {

	public static XmlObject createSpreadsheetWorksheetXML(String title, int rowCount, int columnCount) {
		XmlTag xmlEntry = new XmlTag("entry");
		xmlEntry.setXmlAttribute("xmlns", "http://www.w3.org/2005/Atom");
		xmlEntry.setXmlAttribute("xmlns:gs", "http://schemas.google.com/spreadsheets/2006");
		XmlTag xmlTitle = new XmlTag("title");
		xmlTitle.addXmlObject(new XmlString(title));
		xmlEntry.addXmlObject(xmlTitle);
		XmlTag xmlRowCount = new XmlTag("gs:rowCount");
		xmlRowCount.addXmlObject(new XmlString(Integer.toString(rowCount)));
		xmlEntry.addXmlObject(xmlRowCount);
		XmlTag xmlColumnCount = new XmlTag("gs:colCount");
		xmlColumnCount.addXmlObject(new XmlString(Integer.toString(columnCount)));
		xmlEntry.addXmlObject(xmlColumnCount);
		return xmlEntry;
	}

	private SpreadsheetWorkbook spreadsheetWorkbook;
	private String worksheetId;
	private String gid;
	private String title;
	private int rowCount;
	private int columnCount;
	private URL webURL;

	private void _initial(XmlTag xmlTag) {
		try {
			String id = xmlTag.getXmlTags("id")[0].getInnerString();
			int index = id.lastIndexOf('/');
			this.worksheetId = id.substring(index + 1);
		} catch (Exception ex) {
		}
		try {
			this.title = xmlTag.getXmlTags("title")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.rowCount = Integer.parseInt(xmlTag.getXmlTags("gs:rowCount")[0].getInnerString());
		} catch (Exception ex) {
		}
		try {
			this.columnCount = Integer.parseInt(xmlTag.getXmlTags("gs:colCount")[0].getInnerString());
		} catch (Exception ex) {
		}
		try {
			XmlTag[] links = xmlTag.getXmlTags("link");
			for (XmlTag link : links) {
				String rel = link.getXmlAttribute("rel");
				if (rel.equals("http://schemas.google.com/visualization/2008#visualizationApi")) {
					String href = link.getXmlAttribute("href");
					int index = href.lastIndexOf('=');
					this.gid = href.substring(index + 1);
					this.webURL = new URL("https://docs.google.com/spreadsheets/d/" + this.getSpreadsheetWorkbook().getWorkbookId() + "/edit#gid=" + this.getGid());
				}
			}
		} catch (Exception ex) {
		}
	}

	SpreadsheetWorksheet(SpreadsheetWorkbook spreadsheetWorkbook, XmlTag xmlTag) throws IOException {
		this.spreadsheetWorkbook = spreadsheetWorkbook;
		this._initial(xmlTag);
	}

	public SpreadsheetWorksheet(String workbookId, String worksheetId, GoogleAuthorization googleAuthorization) throws IOException {
		SpreadsheetWorksheet spreadsheetWorksheet = new SpreadsheetWorkbook(workbookId, googleAuthorization).getSpreadsheetWorksheet(worksheetId);
		this.spreadsheetWorkbook = spreadsheetWorksheet.getSpreadsheetWorkbook();
		this.worksheetId = spreadsheetWorksheet.getWorksheetId();
		this.gid = spreadsheetWorksheet.getGid();
		this.title = spreadsheetWorksheet.getTitle();
		this.rowCount = spreadsheetWorksheet.getRowCount();
		this.columnCount = spreadsheetWorksheet.getColumnCount();
		this.webURL = spreadsheetWorksheet.getWebURL();
	}

	SpreadsheetWorkbook getSpreadsheetWorkbook() {
		return this.spreadsheetWorkbook;
	}

	public String getWorksheetId() {
		return this.worksheetId;
	}

	public String getGid() {
		return this.gid;
	}

	public String getTitle() {
		return this.title;
	}

	public int getRowCount() {
		return this.rowCount;
	}

	public int getColumnCount() {
		return this.columnCount;
	}

	public URL getWebURL() {
		return this.webURL;
	}
}
