package mra.google.spreadsheet;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import mra.google.GoogleAuthorization;
import mra.http.HttpRequest;
import mra.xml.XmlObject;
import mra.xml.XmlParser;
import mra.xml.XmlTag;

public class SpreadsheetWorkbook {

	static Map<String, String> createSpreadsheetGoogleAuthorizationHeaders(String contentType, GoogleAuthorization googleAuthorization) {
		Map<String, String> requestHeaders = new LinkedHashMap<>();
		requestHeaders.put("Content-Type", contentType + "; charset=" + HttpRequest.CHARSET_UTF8);
		requestHeaders.put("GData-Version", "3");
		if (googleAuthorization != null) {
			requestHeaders.put("Authorization", googleAuthorization.getToken());
		}
		return requestHeaders;
	}

	public static SpreadsheetWorkbook[] listSpreadsheetWorkbooks(GoogleAuthorization googleAuthorization) throws IOException {
		URL url = new URL("https://spreadsheets.google.com/feeds/spreadsheets");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(SpreadsheetWorkbook.createSpreadsheetGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlObject xmlObject = XmlParser.parse(responseString);
		ArrayList<SpreadsheetWorkbook> spreadsheetWorkbooks = new ArrayList<>();
		XmlTag xmlTag = ((XmlTag) xmlObject);
		XmlTag[] entries = xmlTag.getXmlTags("entry");
		for (XmlTag entry : entries) {
			spreadsheetWorkbooks.add(new SpreadsheetWorkbook(googleAuthorization, entry));
		}
		SpreadsheetWorkbook[] sws = spreadsheetWorkbooks.toArray(new SpreadsheetWorkbook[spreadsheetWorkbooks.size()]);
		return sws;
	}

	private GoogleAuthorization googleAuthorization;
	private String workbookId;
	private String title;
	private URL webURL;

	private void _initial(XmlTag xmlTag) {
		try {
			this.title = xmlTag.getXmlTags("title")[0].getInnerString();
		} catch (Exception ex) {
		}
		try {
			this.webURL = new URL("https://docs.google.com/spreadsheets/d/" + this.getWorkbookId() + "/edit");
		} catch (Exception ex) {
		}
	}

	SpreadsheetWorkbook(GoogleAuthorization googleAuthorization, XmlTag xmlTag) throws IOException {
		this.googleAuthorization = googleAuthorization;
		try {
			String id = xmlTag.getXmlTags("id")[0].getInnerString();
			int index = id.lastIndexOf('/');
			this.workbookId = id.substring(index + 1);
		} catch (Exception ex) {
		}
		this._initial(xmlTag);
	}

	public SpreadsheetWorkbook(String workbookId, GoogleAuthorization googleAuthorization) throws IOException {
		this.googleAuthorization = googleAuthorization;
		URL url = new URL("https://spreadsheets.google.com/feeds/worksheets/" + workbookId + "/private/full");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(SpreadsheetWorkbook.createSpreadsheetGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		this.workbookId = workbookId;
		String responseString = httpRequest.getResponseString();
		XmlObject xmlObject = XmlParser.parse(responseString);
		XmlTag xmlTag = ((XmlTag) xmlObject);
		this._initial(xmlTag);
	}

	GoogleAuthorization getGoogleAuthorization() {
		return this.googleAuthorization;
	}

	public String getWorkbookId() {
		return this.workbookId;
	}

	public String getTitle() {
		return this.title;
	}

	public URL getWebURL() {
		return this.webURL;
	}

	public SpreadsheetWorksheet[] listSpreadsheetWorksheets() throws IOException {
		GoogleAuthorization googleAuthorization = this.getGoogleAuthorization();
		URL url = new URL("https://spreadsheets.google.com/feeds/worksheets/" + this.getWorkbookId() + "/private/full");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(SpreadsheetWorkbook.createSpreadsheetGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlObject xmlObject = XmlParser.parse(responseString);
		XmlTag xmlTag = ((XmlTag) xmlObject);
		ArrayList<SpreadsheetWorksheet> spreadsheetWorksheets = new ArrayList<>();
		XmlTag[] entries = xmlTag.getXmlTags("entry");
		for (XmlTag entry : entries) {
			spreadsheetWorksheets.add(new SpreadsheetWorksheet(this, entry));
		}
		SpreadsheetWorksheet[] sws = spreadsheetWorksheets.toArray(new SpreadsheetWorksheet[spreadsheetWorksheets.size()]);
		return sws;
	}

	public SpreadsheetWorksheet getSpreadsheetWorksheet(String worksheetId) throws IOException {
		GoogleAuthorization googleAuthorization = this.getGoogleAuthorization();
		URL url = new URL("https://spreadsheets.google.com/feeds/worksheets/" + this.getWorkbookId() + "/private/full/" + worksheetId);
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(SpreadsheetWorkbook.createSpreadsheetGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		return new SpreadsheetWorksheet(this, xmlTag);
	}

	public SpreadsheetWorksheet addSpreadsheetWorksheet(String title, int rowCount, int columnCount) throws IOException {
		GoogleAuthorization googleAuthorization = this.getGoogleAuthorization();
		URL url = new URL("https://spreadsheets.google.com/feeds/worksheets/" + this.getWorkbookId() + "/private/full");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_POST);
		httpRequest.setRequestHeaders(SpreadsheetWorkbook.createSpreadsheetGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_ATOM_XML, googleAuthorization));
		String requestData = SpreadsheetWorksheet.createSpreadsheetWorksheetXML(title, rowCount, columnCount).toString();
		httpRequest.setRequestData(requestData);
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		XmlTag xmlTag = XmlParser.parse(responseString);
		return new SpreadsheetWorksheet(this, xmlTag);
	}
}
