package mra.google.youtube;

import java.io.IOException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import mra.http.HttpRequest;
import mra.google.GoogleAuthorization;
import mra.xml.XmlString;
import mra.xml.XmlTag;

public class YoutubeEntry {

	public static String createYoutubeEntryXML(String videoId, Integer position) {
		XmlTag xmlRoot = new XmlTag("entry");
		xmlRoot.setXmlAttribute("xmlns", "http://www.w3.org/2005/Atom");
		xmlRoot.setXmlAttribute("xmlns:yt", "http://gdata.youtube.com/schemas/2007");
		StringBuilder sb = new StringBuilder();
		if (videoId != null) {
			XmlTag xmlChild = new XmlTag("id");
			xmlChild.addXmlObject(new XmlString(videoId));
			xmlRoot.addXmlObject(xmlChild);
		}
		if (position != null && position > 0) {
			XmlTag xmlChild = new XmlTag("yt:position");
			xmlChild.addXmlObject(new XmlString(position.toString()));
			xmlRoot.addXmlObject(xmlChild);
		}
		return xmlRoot.toString();
	}
	private YoutubePlaylist youtubePlaylist;
	private YoutubeVideo youtubeVideo;
	private String entryId;
	private int position;
	private URL feedURL;

	YoutubePlaylist getYoutubePlaylist() {
		return this.youtubePlaylist;
	}

	YoutubeVideo getYoutubeVideo() {
		return this.youtubeVideo;
	}

	public String getTitle() {
		return this.getYoutubeVideo().getTitle();
	}

	public String getEntryId() {
		return this.entryId;
	}

	public int getPosition() {
		return this.position;
	}

	URL getFeedURL() {
		return this.feedURL;
	}

	YoutubeEntry(YoutubePlaylist youtubePlaylist, XmlTag xmlTag) {
		this.youtubePlaylist = youtubePlaylist;
		this.youtubeVideo = new YoutubeVideo(youtubePlaylist.getYoutubeChannel(), xmlTag);
		try {
			String id = xmlTag.getXmlTags("id")[0].getInnerString();
			int index = id.lastIndexOf('/');
			this.entryId = id.substring(index + 1);
			this.feedURL = new URL("https://gdata.youtube.com/feeds/api/playlists/" + this.getYoutubePlaylist().getPlaylistId() + "/" + this.getEntryId());
		} catch (Exception ex) {
		}
		try {
			this.position = Integer.parseInt(xmlTag.getXmlTags("yt:position")[0].getInnerString());
		} catch (Exception ex) {
		}
	}

	public void upadte(Integer position) throws IOException {
		GoogleAuthorization googleAuthorization = this.getYoutubePlaylist().getYoutubeChannel().getGoogleAuthorization();
		URL url = this.getFeedURL();
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_PUT);
		if (googleAuthorization != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_WWW_FORM + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", googleAuthorization.getToken());
			requestHeaders.put("X-GData-Client", googleAuthorization.getClientId());
			requestHeaders.put("X-GData-Key", String.format("key=\"%s\"", googleAuthorization.getApiKey()));
			httpRequest.setRequestHeaders(requestHeaders);
		}
		if (position != null) {
			this.position = position;
		}
		String requestData = YoutubeEntry.createYoutubeEntryXML(null, this.position);
		httpRequest.setRequestData(requestData);
		httpRequest.send();
	}

	public void updatePosition(int position) throws IOException {
		this.upadte(position);
	}

	public void remove() throws IOException {
		GoogleAuthorization googleAuthorization = this.getYoutubePlaylist().getYoutubeChannel().getGoogleAuthorization();
		URL url = this.getFeedURL();
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_DELETE);
		if (googleAuthorization != null) {
			Map<String, String> requestHeaders = new LinkedHashMap<>();
			requestHeaders.put("Content-Type", HttpRequest.CONTENT_TYPE_WWW_FORM + "; charset=" + HttpRequest.CHARSET_UTF8);
			requestHeaders.put("Authorization", googleAuthorization.getToken());
			requestHeaders.put("X-GData-Client", googleAuthorization.getClientId());
			requestHeaders.put("X-GData-Key", String.format("key=\"%s\"", googleAuthorization.getApiKey()));
			httpRequest.setRequestHeaders(requestHeaders);
		}
		httpRequest.send();
		this.youtubePlaylist = null;
		this.youtubeVideo = null;
		this.entryId = null;
		this.position = 0;
		this.feedURL = null;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Entry ID = ");
		sb.append(this.getEntryId());
		sb.append("\nTitle = ");
		sb.append(this.getTitle());
		sb.append("\nPosition = ");
		sb.append(this.getPosition());
		if (this.getYoutubeVideo() != null) {
			sb.append("\n");
			sb.append(this.getYoutubeVideo().toString());
		}
		return sb.toString();
	}
}
