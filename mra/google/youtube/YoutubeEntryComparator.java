package mra.google.youtube;

import java.util.Comparator;

public class YoutubeEntryComparator implements Comparator<YoutubeEntry> {

	public static final char SORT_BY_TITLE = 'T';
	public static final char SORT_BY_POSITION = 'P';
	public static final char ORDER_BY_ASCENDING = 'A';
	public static final char ORDER_BY_DESCENDING = 'D';
	private char sortBy;
	private char orderBy;

	public YoutubeEntryComparator(char sortBy, char orderBy) {
		switch (sortBy) {
			case YoutubeEntryComparator.SORT_BY_TITLE: {
				this.sortBy = sortBy;
			}
			break;
			default: {
				this.sortBy = YoutubeEntryComparator.SORT_BY_POSITION;
			}
		}
		switch (orderBy) {
			case YoutubeEntryComparator.ORDER_BY_DESCENDING: {
				this.orderBy = orderBy;
			}
			break;
			default: {
				this.orderBy = YoutubeEntryComparator.ORDER_BY_ASCENDING;
			}
		}
	}

	public YoutubeEntryComparator() {
		this(YoutubeEntryComparator.SORT_BY_POSITION, YoutubeEntryComparator.ORDER_BY_ASCENDING);
	}

	@Override
	public int compare(YoutubeEntry t1, YoutubeEntry t2) {
		switch (this.sortBy) {
			case YoutubeEntryComparator.SORT_BY_TITLE: {
				switch (this.orderBy) {
					case YoutubeVideoComparator.ORDER_BY_DESCENDING: {
						return t2.getTitle().compareTo(t1.getTitle());
					}
					default: {
						return t1.getTitle().compareTo(t2.getTitle());
					}
				}
			}
			default: {
				switch (this.orderBy) {
					case YoutubeVideoComparator.ORDER_BY_DESCENDING: {
						return t2.getPosition() - t1.getPosition();
					}
					default: {
						return t1.getPosition() - t2.getPosition();
					}
				}
			}
		}
	}
}
