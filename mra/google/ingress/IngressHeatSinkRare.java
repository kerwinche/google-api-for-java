package mra.google.ingress;

public class IngressHeatSinkRare extends IngressHeatSinks {

	IngressHeatSinkRare(String guid, IngressAgent owner, int removalStickiness) {
		super(guid, owner, 80, "Heat Sink", "Rare", 800, removalStickiness, 500000);
	}
}
