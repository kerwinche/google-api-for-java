package mra.google.ingress;

public class IngressResonatorL2 extends IngressResonators {

	IngressResonatorL2(String guid, IngressAgent owner) {
		this(guid, owner, -1, -1);
	}

	IngressResonatorL2(String guid, IngressAgent owner, int remainedEnergy, int distance) {
		super(guid, owner, 2, 1500, remainedEnergy, distance);
	}
}
