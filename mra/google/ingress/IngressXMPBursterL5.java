package mra.google.ingress;

public class IngressXMPBursterL5 extends IngressXMPBursters {

	IngressXMPBursterL5(String guid, IngressAgent owner) {
		super(guid, owner, 5, 1200, 90);
	}
}
