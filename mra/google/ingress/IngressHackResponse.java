package mra.google.ingress;

public class IngressHackResponse {

	private final long damage;
	private final int remainedEnergy;
	private final String status;
	private final IngressInventory[] ingressInventories;

	IngressHackResponse(long damage, int remainedEnergy, String status, IngressInventory[] ingressInventories) {
		this.damage = damage;
		this.remainedEnergy = remainedEnergy;
		this.status = status;
		this.ingressInventories = ingressInventories;
	}

	public long getDamage() {
		return this.damage;
	}

	public int getRemainedEnergy() {
		return this.remainedEnergy;
	}

	public String getStatus() {
		return this.status;
	}

	public IngressInventory[] getIngressInventories() {
		return this.ingressInventories;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Damage = ");
		sb.append(this.getDamage());
		sb.append("\nRemained Energy = ");
		sb.append(this.getRemainedEnergy());
		sb.append("\nStatus = ");
		sb.append(this.getStatus());
		sb.append("\nIngress Inventory = {\n");
		for (IngressInventory ingressInventory : this.getIngressInventories()) {
			sb.append(ingressInventory.toString());
		}
		sb.append("\n}");
		return sb.toString();
	}
}
