package mra.google.ingress;

public class IngressUltraStrikeL8 extends IngressUltraStrikes {

	IngressUltraStrikeL8(String guid, IngressAgent owner) {
		super(guid, owner, 8, 2700, 30, 640);
	}
}
