package mra.google.ingress;

public class IngressUltraStrikeL3 extends IngressUltraStrikes {

	IngressUltraStrikeL3(String guid, IngressAgent owner) {
		super(guid, owner, 3, 500, 16, 70);
	}
}
