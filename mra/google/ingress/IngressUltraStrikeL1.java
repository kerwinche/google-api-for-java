package mra.google.ingress;

public class IngressUltraStrikeL1 extends IngressUltraStrikes {

	IngressUltraStrikeL1(String guid, IngressAgent owner) {
		super(guid, owner, 1, 150, 10, 10);
	}
}
