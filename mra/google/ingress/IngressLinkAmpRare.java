package mra.google.ingress;

public class IngressLinkAmpRare extends IngressLinkAmps {

	IngressLinkAmpRare(String guid, IngressAgent owner, int removalStickiness) {
		super(guid, owner, 80, "Link Amp", "Rare", 800, removalStickiness, 2000);
	}
}
