package mra.google.ingress;

public class IngressXMPBursterL4 extends IngressXMPBursters {

	IngressXMPBursterL4(String guid, IngressAgent owner) {
		super(guid, owner, 4, 900, 72);
	}
}
