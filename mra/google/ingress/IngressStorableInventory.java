package mra.google.ingress;

public abstract class IngressStorableInventory extends IngressInventory {

	IngressStorableInventory(String guid, IngressAgent owner, int recycle, String name, String rarity) {
		super(guid, owner, recycle, name, rarity);
	}
}
