package mra.google.ingress;

public abstract class IngressInventory extends IngressNonAgentObject {

	private final int recycle;
	private final String name;
	private final String rarity;

	IngressInventory(String guid, IngressAgent owner, int recycle, String name, String rarity) {
		super(guid, owner);
		this.recycle = recycle;
		this.name = name;
		this.rarity = rarity;
	}

	public int getRecycle() {
		return this.recycle;
	}

	public String getName() {
		return this.name;
	}

	public String getRarity() {
		return this.rarity;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nRecycle = ");
		sb.append(this.getRecycle());
		sb.append("\nName = ");
		sb.append(this.getName());
		sb.append("\nRarity = ");
		sb.append(this.getRarity());
		return sb.toString();
	}
}
