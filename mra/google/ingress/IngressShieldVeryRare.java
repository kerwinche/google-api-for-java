package mra.google.ingress;

public class IngressShieldVeryRare extends IngressShields {

	IngressShieldVeryRare(String guid, IngressAgent owner, int removalStickiness) {
		super(guid, owner, 100, "Shield", "Very Rare", 1000, removalStickiness, 60);
	}
}
