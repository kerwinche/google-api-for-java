package mra.google.ingress;

public abstract class IngressHeatSinks extends IngressMods {

	static IngressHeatSinks createIngressHeatSinks(String guid, IngressAgent owner, String rarity, int removalStickiness) {
		if (rarity.equals("COMMON")) {
			return new IngressHeatSinkCommon(guid, owner, removalStickiness);
		} else if (rarity.equals("RARE")) {
			return new IngressHeatSinkRare(guid, owner, removalStickiness);
		} else if (rarity.equals("VERY_RARE")) {
			return new IngressHeatSinkVeryRare(guid, owner, removalStickiness);
		}
		return null;
	}

	private final int hackSpeed;

	IngressHeatSinks(String guid, IngressAgent owner, int recycle, String name, String rarity, int cost, int removalStickiness, int hackSpeed) {
		super(guid, owner, recycle, name, rarity, cost, removalStickiness);
		this.hackSpeed = hackSpeed;
	}

	public int getHackSpeed() {
		return this.hackSpeed;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nHack Speed = ");
		sb.append(this.getHackSpeed());
		return sb.toString();
	}
}
