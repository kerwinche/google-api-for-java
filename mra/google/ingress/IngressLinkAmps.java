package mra.google.ingress;

public abstract class IngressLinkAmps extends IngressMods {

	static IngressLinkAmps createIngressLinkAmps(String guid, IngressAgent owner, String rarity, int removalStickiness) {
		if (rarity.equals("RARE")) {
			return new IngressLinkAmpRare(guid, owner, removalStickiness);
		} else if (rarity.equals("VERY_RARE")) {
			return new IngressLinkAmpVeryRare(guid, owner, removalStickiness);
		}
		return null;
	}

	private final int linkRangeMultiplier;

	IngressLinkAmps(String guid, IngressAgent owner, int recycle, String name, String rarity, int cost, int removalStickiness, int linkRangeMultiplier) {
		super(guid, owner, recycle, name, rarity, cost, removalStickiness);
		this.linkRangeMultiplier = linkRangeMultiplier;
	}

	public int getLinkRangeMultiplier() {
		return this.linkRangeMultiplier;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nLink Range Multiplier");
		sb.append(this.getLinkRangeMultiplier());
		return sb.toString();
	}
}
