package mra.google.ingress;

public class IngressMultihackCommon extends IngressMultihacks {

	IngressMultihackCommon(String guid, IngressAgent owner, int removalStickiness) {
		super(guid, owner, 40, "Multi-hack", "Common", 400, removalStickiness, 4);
	}
}
