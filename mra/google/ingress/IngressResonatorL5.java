package mra.google.ingress;

public class IngressResonatorL5 extends IngressResonators {

	IngressResonatorL5(String guid, IngressAgent owner) {
		this(guid, owner, -1, -1);
	}

	IngressResonatorL5(String guid, IngressAgent owner, int remainedEnergy, int distance) {
		super(guid, owner, 5, 3000, remainedEnergy, distance);
	}
}
