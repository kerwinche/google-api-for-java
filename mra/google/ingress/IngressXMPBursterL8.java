package mra.google.ingress;

public class IngressXMPBursterL8 extends IngressXMPBursters {

	IngressXMPBursterL8(String guid, IngressAgent owner) {
		super(guid, owner, 8, 2700, 168);
	}
}
