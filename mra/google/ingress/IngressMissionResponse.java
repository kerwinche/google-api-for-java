package mra.google.ingress;

public class IngressMissionResponse {

	private final String guid;
	private final int waypoints;

	IngressMissionResponse(String guid, int waypoints) {
		this.guid = guid;
		this.waypoints = waypoints;
	}

	public String getGuid() {
		return this.guid;
	}

	public int countWaypoints() {
		return this.waypoints;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Ingress Mission Guid = ");
		sb.append(this.getGuid());
		sb.append("\nIngress Waypoints Count = ");
		sb.append(this.countWaypoints());
		return sb.toString();
	}
}
