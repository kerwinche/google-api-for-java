package mra.google.ingress;

import java.util.HashMap;
import java.util.Map;

public class IngressWaypoint extends IngressPortal {

	public static final String WAYPOINT_TYPE_HACK = "Hack this Portal";
	public static final String WAYPOINT_TYPE_CAPTURE_OR_UPGRADE = "Capture or Upgrade Portal";
	public static final String WAYPOINT_TYPE_CREATE_LINK = "Create Link from Portal";
	public static final String WAYPOINT_TYPE_CREATE_FIELD = "Create Field from Portal";
	public static final String WAYPOINT_TYPE_INSTALL_MOD = "Install a Mod on this Portal";
	public static final String WAYPOINT_TYPE_VIEW = "View this Field Trip Waypoint";
	public static final String WAYPOINT_TYPE_ENTER_THE_PASSPHRASE = "Enter the Passphrase";
	private final String type;
	private final String question;
	static final int $WAYPOINT_TYPE_HACK = 1;
	static final int $WAYPOINT_TYPE_CAPTURE_OR_UPGRADE = 2;
	static final int $WAYPOINT_TYPE_CREATE_LINK = 3;
	static final int $WAYPOINT_TYPE_CREATE_FIELD = 4;
	static final int $WAYPOINT_TYPE_INSTALL_MOD = 5;
	static final int $WAYPOINT_TYPE_VIEW = 7;
	static final int $WAYPOINT_TYPE_ENTER_THE_PASSPHRASE = 8;
	static final Map<Integer, String> WAYPOINY_TYPES_MAP = new HashMap<>();

	static {
		IngressWaypoint.WAYPOINY_TYPES_MAP.put(IngressWaypoint.$WAYPOINT_TYPE_HACK, IngressWaypoint.WAYPOINT_TYPE_HACK);
		IngressWaypoint.WAYPOINY_TYPES_MAP.put(IngressWaypoint.$WAYPOINT_TYPE_CAPTURE_OR_UPGRADE, IngressWaypoint.WAYPOINT_TYPE_CAPTURE_OR_UPGRADE);
		IngressWaypoint.WAYPOINY_TYPES_MAP.put(IngressWaypoint.$WAYPOINT_TYPE_CREATE_LINK, IngressWaypoint.WAYPOINT_TYPE_CREATE_LINK);
		IngressWaypoint.WAYPOINY_TYPES_MAP.put(IngressWaypoint.$WAYPOINT_TYPE_CREATE_FIELD, IngressWaypoint.WAYPOINT_TYPE_CREATE_FIELD);
		IngressWaypoint.WAYPOINY_TYPES_MAP.put(IngressWaypoint.$WAYPOINT_TYPE_INSTALL_MOD, IngressWaypoint.WAYPOINT_TYPE_INSTALL_MOD);
		IngressWaypoint.WAYPOINY_TYPES_MAP.put(IngressWaypoint.$WAYPOINT_TYPE_VIEW, IngressWaypoint.WAYPOINT_TYPE_VIEW);
		IngressWaypoint.WAYPOINY_TYPES_MAP.put(IngressWaypoint.$WAYPOINT_TYPE_ENTER_THE_PASSPHRASE, IngressWaypoint.WAYPOINT_TYPE_ENTER_THE_PASSPHRASE);
	}

	IngressWaypoint(IngressPortal ingressPortal, String type, String question) {
		this(ingressPortal.getGuid(), ingressPortal.getOwner(), ingressPortal.getLatitude(), ingressPortal.getLongitude(), ingressPortal.getAddress(), ingressPortal.getTitle(), ingressPortal.getDescription(), ingressPortal.getLevel(), ingressPortal.getEnergyPercentage(), ingressPortal.getFaction(), ingressPortal.getDiscoverer(), ingressPortal.getCover(), ingressPortal.getIngressResonatorses(), ingressPortal.getIngressModses(), ingressPortal.getIngressLinksOut(), ingressPortal.getIngressLinksIn(), type, question);
	}

	IngressWaypoint(String guid, IngressAgent owner, long latitude, long longitude, String address, String title, String description, int level, int energy, String faction, IngressAgent discoverer, IngressImage cover, IngressResonators[] ingressResonatorses, IngressMods[] ingressModses, IngressLink[] ingressLinksOut, IngressLink[] ingressLinksIn, String type, String question) {
		super(guid, owner, latitude, longitude, address, title, description, level, energy, faction, discoverer, cover, ingressResonatorses, ingressModses, ingressLinksOut, ingressLinksIn);
		this.type = type;
		this.question = question;
	}

	public String getType() {
		return this.type;
	}

	public String getQuestion() {
		return this.question;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nType = ");
		sb.append(this.getType());
		sb.append("\nQuestion = ");
		sb.append(this.getQuestion() == null ? "<null>" : this.getQuestion());
		return sb.toString();
	}
}
