package mra.google.ingress;

public class IngressPowerCubeL1 extends IngressPowerCubes {

	IngressPowerCubeL1(String guid, IngressAgent owner) {
		super(guid, owner, 1);
	}
}
