package mra.google.ingress;

public class IngressShieldCommon extends IngressShields {

	IngressShieldCommon(String guid, IngressAgent owner, int removalStickiness) {
		super(guid, owner, 40, "Shield", "Common", 400, removalStickiness, 30);
	}
}
