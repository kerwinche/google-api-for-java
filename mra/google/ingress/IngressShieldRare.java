package mra.google.ingress;

public class IngressShieldRare extends IngressShields {

	IngressShieldRare(String guid, IngressAgent owner, int removalStickiness) {
		super(guid, owner, 80, "Shield", "Rare", 800, removalStickiness, 40);
	}
}
