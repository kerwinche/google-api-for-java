package mra.google.ingress;

public class IngressResonatorL1 extends IngressResonators {

	IngressResonatorL1(String guid, IngressAgent owner) {
		this(guid, owner, -1, -1);
	}

	IngressResonatorL1(String guid, IngressAgent owner, int remainedEnergy, int distance) {
		super(guid, owner, 1, 1000, remainedEnergy, distance);
	}
}
