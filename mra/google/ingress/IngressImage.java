package mra.google.ingress;

import java.net.URL;

public class IngressImage extends IngressNonAgentObject {

	private final URL imagePath;

	IngressImage(String guid, IngressAgent owner, URL imagePath) {
		super(guid, owner);
		this.imagePath = imagePath;
	}

	public URL getImagePath() {
		return this.imagePath;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nImage Path = ");
		sb.append(this.getImagePath() == null ? "<null>" : this.getImagePath().toString());
		return sb.toString();
	}
}
