package mra.google.ingress;

public class IngressFireResponse {

	private final long damage;
	private final long apGained;
	private final int remainedEnergy;
	private final String status;

	IngressFireResponse(long damage, long apGained, int remainedEnergy, String status) {
		this.damage = damage;
		this.apGained = apGained;
		this.remainedEnergy = remainedEnergy;
		this.status = status;
	}

	public long getDamage() {
		return this.damage;
	}

	public long getAPGained() {
		return this.apGained;
	}

	public int getRemainedEnergy() {
		return this.remainedEnergy;
	}

	public String getStatus() {
		return this.status;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Damage = ");
		sb.append(this.getDamage());
		sb.append("\nAP Gained = ");
		sb.append(this.getAPGained());
		sb.append("\nRemained Energy = ");
		sb.append(this.getRemainedEnergy());
		sb.append("\nStatus = ");
		sb.append(this.getStatus());
		return sb.toString();
	}
}
