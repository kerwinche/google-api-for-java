package mra.google.ingress;

public abstract class IngressMultihacks extends IngressMods {

	static IngressMultihacks createIngressMultihacks(String guid, IngressAgent owner, String rarity, int removalStickiness) {
		if (rarity.equals("COMMON")) {
			return new IngressMultihackCommon(guid, owner, removalStickiness);
		} else if (rarity.equals("RARE")) {
			return new IngressMultihackRare(guid, owner, removalStickiness);
		} else if (rarity.equals("VERY_RARE")) {
			return new IngressMultihackVeryRare(guid, owner, removalStickiness);
		}
		return null;
	}
	private final int burnoutInsulation;

	IngressMultihacks(String guid, IngressAgent owner, int recycle, String name, String rarity, int cost, int removalStickiness, int burnoutInsulation) {
		super(guid, owner, recycle, name, rarity, cost, removalStickiness);
		this.burnoutInsulation = burnoutInsulation;
	}

	public int getBurnoutInsulation() {
		return this.burnoutInsulation;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nBurnout Insulation");
		sb.append(this.getBurnoutInsulation());
		return sb.toString();
	}
}
