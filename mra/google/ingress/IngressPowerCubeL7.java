package mra.google.ingress;

public class IngressPowerCubeL7 extends IngressPowerCubes {

	IngressPowerCubeL7(String guid, IngressAgent owner) {
		super(guid, owner, 7);
	}
}
