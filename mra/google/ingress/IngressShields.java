package mra.google.ingress;

public abstract class IngressShields extends IngressMods {

	static IngressShields createIngressShields(String guid, IngressAgent owner, String rarity, int removalStickiness) {
		if (rarity.equals("COMMON")) {
			return new IngressShieldCommon(guid, owner, removalStickiness);
		} else if (rarity.equals("RARE")) {
			return new IngressShieldRare(guid, owner, removalStickiness);
		} else if (rarity.equals("VERY_RARE")) {
			return new IngressShieldVeryRare(guid, owner, removalStickiness);
		}
		return null;
	}

	private final int mitigation;

	IngressShields(String guid, IngressAgent owner, int recycle, String name, String rarity, int cost, int removalStickiness, int mitigation) {
		super(guid, owner, recycle, name, rarity, cost, removalStickiness);
		this.mitigation = mitigation;
	}

	public int getMitigation() {
		return this.mitigation;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nMitigation = ");
		sb.append(this.getMitigation());
		return sb.toString();
	}
}
