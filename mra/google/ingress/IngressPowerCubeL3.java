package mra.google.ingress;

public class IngressPowerCubeL3 extends IngressPowerCubes {

	IngressPowerCubeL3(String guid, IngressAgent owner) {
		super(guid, owner, 3);
	}
}
