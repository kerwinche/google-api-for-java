package mra.google.ingress;

public class IngressCapsule extends IngressCapsules {

	IngressCapsule(String guid, IngressAgent owner) {
		super(guid, owner, 80, "Capsule", "Rare");
	}
}
