package mra.google.ingress;

public abstract class IngressWeapons extends IngressLevelableInventory {

	private final int damage;
	private final int radius;
	private final int cost;

	IngressWeapons(String guid, IngressAgent owner, String name, int level, int damage, int radius, int cost) {
		super(guid, owner, name, level);
		this.damage = damage;
		this.radius = radius;
		this.cost = cost;
	}

	public int getDamage() {
		return this.damage;
	}

	public int getRadius() {
		return this.radius;
	}

	public int getCost() {
		return this.cost;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nDamage = ");
		sb.append(this.getDamage());
		sb.append("\nRadius = ");
		sb.append(this.getRadius());
		sb.append("\nCost = ");
		sb.append(this.getCost());
		return sb.toString();
	}
}
