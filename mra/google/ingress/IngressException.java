package mra.google.ingress;

public class IngressException extends RuntimeException {

	public IngressException() {
	}

	public IngressException(String message) {
		super(message);
	}

	public IngressException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public IngressException(Throwable cause) {
		super(cause);
	}

	public IngressException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}
}
