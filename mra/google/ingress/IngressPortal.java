package mra.google.ingress;

public class IngressPortal extends IngressNonAgentObject {

	static final Class[] ingressLinkAmpsClasses = {IngressLinkAmpVeryRare.class, IngressSoftBankUltraLink.class, IngressLinkAmpRare.class};
	static final Class[] ingressHeatSinksClasses = {IngressHeatSinkVeryRare.class, IngressHeatSinkRare.class, IngressHeatSinkCommon.class};
	static final Class[] ingressMultihacksClasses = {IngressMultihackVeryRare.class, IngressMultihackRare.class, IngressMultihackCommon.class};
	private final long latitude;
	private final long longitude;
	private final String address;
	private final String title;
	private final String description;
	private final int level;
	private final int energy;
	private final String faction;
	private final IngressAgent discoverer;
	private final IngressImage cover;
	private final IngressResonators[] ingressResonatorses;
	private final IngressMods[] ingressModses;
	private final IngressLink[] ingressLinksOut;
	private final IngressLink[] ingressLinksIn;

	IngressPortal(String guid, IngressAgent owner, long latitude, long longitude, String address, String title, String description, int level, int energy, String faction, IngressAgent discoverer, IngressImage cover, IngressResonators[] ingressResonatorses, IngressMods[] ingressModses, IngressLink[] ingressLinksOut, IngressLink[] ingressLinksIn) {
		super(guid, owner);
		this.latitude = latitude;
		this.longitude = longitude;
		this.address = address;
		this.title = title;
		this.description = description;
		this.level = level;
		this.energy = energy;
		this.faction = faction;
		this.discoverer = discoverer;
		this.cover = cover;
		this.ingressResonatorses = ingressResonatorses;
		this.ingressModses = ingressModses;
		this.ingressLinksOut = ingressLinksOut;
		this.ingressLinksIn = ingressLinksIn;
	}

	public long getLatitude() {
		return this.latitude;
	}

	public long getLongitude() {
		return this.longitude;
	}

	public String getAddress() {
		return this.address;
	}

	public String getTitle() {
		return this.title;
	}

	public String getDescription() {
		return this.description;
	}

	public String getFaction() {
		return this.faction;
	}

	public IngressAgent getDiscoverer() {
		return this.discoverer;
	}

	public IngressImage getCover() {
		return this.cover;
	}

	public IngressResonators[] getIngressResonatorses() {
		return this.ingressResonatorses;
	}

	public IngressMods[] getIngressModses() {
		return this.ingressModses;
	}

	public IngressLink[] getIngressLinksOut() {
		return this.ingressLinksOut;
	}

	public IngressLink[] getIngressLinksIn() {
		return this.ingressLinksIn;
	}

	private int getIngressResonatorsLevels() {
		int value = 0;
		if (this.getIngressResonatorses() != null) {
			for (IngressResonators ingressResonators : this.getIngressResonatorses()) {
				if (ingressResonators != null) {
					value += ingressResonators.getLevel();
				}
			}
		}
		return value;
	}

	public int getLevel() {
		int level = this.level;
		if (level == -1) {
			level = this.getIngressResonatorsLevels() / 8;
			if (level == 0) {
				level = 1;
			}
		}
		return level;
	}

	public double getMaximumLinkableDistance() {
		int value = this.getIngressResonatorsLevels();
		double distance = 160 * Math.pow(value / 8.0, 4);
		int linkAmps = 0;
		int totalMultiplier = 0;
		for (Class ingressLinkAmpsClass : IngressPortal.ingressLinkAmpsClasses) {
			if (this.getIngressModses() != null) {
				for (IngressMods ingressMods : this.getIngressModses()) {
					if (ingressLinkAmpsClass.isInstance(ingressMods)) {
						linkAmps++;
						IngressLinkAmps ingressLinkAmps = ((IngressLinkAmps) ingressMods);
						int multiplier = ingressLinkAmps.getLinkRangeMultiplier();
						if (linkAmps == 1) {
							totalMultiplier += multiplier;
						} else if (linkAmps == 2) {
							totalMultiplier += multiplier / 4;
						} else if (linkAmps > 2) {
							totalMultiplier += multiplier / 8;
						}
					}
				}
			}
		}
		if (linkAmps > 0) {
			distance *= (totalMultiplier / 1000.0);
		}
		return distance;
	}

	public int getOriginalEnergy() {
		int enery = 0;
		if (this.getIngressResonatorses() != null) {
			for (IngressResonators ingressResonators : this.getIngressResonatorses()) {
				if (ingressResonators != null) {
					enery += ingressResonators.getOriginalEnergy();
				}
			}
		}
		return enery;
	}

	public int getRemainedEnergy() {
		int enery = 0;
		if (this.getIngressResonatorses() != null) {
			for (IngressResonators ingressResonators : this.getIngressResonatorses()) {
				if (ingressResonators != null) {
					enery += ingressResonators.getRemainedEnergy();
				}
			}
		}
		return enery;
	}

	public int getEnergyPercentage() {
		int energy = this.energy;
		if (energy == -1) {
			energy = new Double(Math.ceil(100.0 * this.getRemainedEnergy() / this.getOriginalEnergy())).intValue();
		}
		return energy;
	}

	public int getDamageReducing() {
		int mitigation = 0;
		if (this.getIngressModses() != null) {
			for (IngressMods ingressMods : this.getIngressModses()) {
				if (ingressMods instanceof IngressShields) {
					IngressShields ingressShields = ((IngressShields) ingressMods);
					mitigation += ingressShields.getMitigation();
				}
			}
		}
		return mitigation;
	}

	public int getCoolDownTime() {
		int mitigation = 300000;
		int heatSinks = 0;
		int totalHackSpeed = 1000000;
		for (Class ingressHeatSinksClass : IngressPortal.ingressHeatSinksClasses) {
			if (this.getIngressModses() != null) {
				for (IngressMods ingressMods : this.getIngressModses()) {
					if (ingressHeatSinksClass.isInstance(ingressMods)) {
						heatSinks++;
						IngressHeatSinks ingressHeatSinks = ((IngressHeatSinks) ingressMods);
						int hackSpeed = ingressHeatSinks.getHackSpeed();
						if (heatSinks == 1) {
							totalHackSpeed += hackSpeed;
						} else if (heatSinks > 1) {
							totalHackSpeed += hackSpeed / 2;
						}
					}
				}
			}
		}
		if (heatSinks > 0) {
			mitigation *= (1000000.0 / totalHackSpeed);
		}
		return mitigation;
	}

	public int getHackTimes() {
		int totalBurnoutInsulation = 4;
		int multihacks = 0;
		for (Class ingressMultihacksClass : IngressPortal.ingressMultihacksClasses) {
			if (this.getIngressModses() != null) {
				for (IngressMods ingressMods : this.getIngressModses()) {
					if (ingressMultihacksClass.isInstance(ingressMods)) {
						multihacks++;
						IngressMultihacks ingressMultihacks = ((IngressMultihacks) ingressMods);
						int burnoutInsulation = ingressMultihacks.getBurnoutInsulation();
						if (multihacks == 1) {
							totalBurnoutInsulation += burnoutInsulation;
						} else if (multihacks > 1) {
							totalBurnoutInsulation += burnoutInsulation / 2;
						}
					}
				}
			}
		}
		return totalBurnoutInsulation;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nLatitude = ");
		sb.append(this.getLatitude());
		sb.append("\nLongitude = ");
		sb.append(this.getLongitude());
		sb.append("\nAddress = ");
		sb.append(this.getAddress());
		sb.append("\nTitle = ");
		sb.append(this.getTitle());
		sb.append("\nDescription = ");
		sb.append(this.getDescription());
		sb.append("\nFaction = ");
		sb.append(this.getFaction());
		sb.append("\nDiscoverer = {\n");
		sb.append(this.getDiscoverer() == null ? "<null>" : this.getDiscoverer().toString());
		sb.append("\n}");
		sb.append("\nCover = {\n");
		sb.append(this.getCover() == null ? "<null>" : this.getCover().toString());
		sb.append("\n}");
		sb.append("\nIngress Resonators = {");
		if (this.ingressResonatorses != null) {
			for (int i = 0; i < this.ingressResonatorses.length; i++) {
				if (i > 0) {
					sb.append(",");
				}
				sb.append("\n{\n");
				IngressResonators ingressResonators = this.ingressResonatorses[i];
				sb.append(ingressResonators == null ? "<null>" : ingressResonators.toString());
				sb.append("\n}");
			}
		}
		sb.append("\n}");
		sb.append("\nLevel = ");
		sb.append(this.getLevel());
		sb.append("\nOriginal Energy = ");
		sb.append(this.getOriginalEnergy());
		sb.append("\nRemained Energy = ");
		sb.append(this.getRemainedEnergy());
		sb.append("\nEnergy Percentage = ");
		sb.append(this.getEnergyPercentage());
		sb.append("\nIngress Mods = {");
		if (this.ingressModses != null) {
			for (int i = 0; i < this.ingressModses.length; i++) {
				if (i > 0) {
					sb.append(",");
				}
				sb.append("\n{\n");
				IngressMods ingressMods = this.ingressModses[i];
				sb.append(ingressMods == null ? "<null>" : ingressMods.toString());
				sb.append("\n}");
			}
		}
		sb.append("\n}");
		sb.append("\nCool Down Time = ");
		sb.append(this.getCoolDownTime());
		sb.append("\nHack Times Within 4 Hours = ");
		sb.append(this.getHackTimes());
		sb.append("\nDamage Reducing = ");
		sb.append(this.getDamageReducing());
		sb.append("\nMaximum Linkable Distance = ");
		sb.append(this.getMaximumLinkableDistance());
		sb.append("\nIngress Links Out = {");
		if (this.ingressLinksOut != null) {
			for (int i = 0; i < this.ingressLinksOut.length; i++) {
				if (i > 0) {
					sb.append(",");
				}
				sb.append("\n{\n");
				IngressLink ingressLink = this.ingressLinksOut[i];
				sb.append(ingressLink == null ? "<null>" : ingressLink.toString());
				sb.append("\n}");
			}
		}
		sb.append("\n}");
		sb.append("\nIngress Links In = {");
		if (this.ingressLinksIn != null) {
			for (int i = 0; i < this.ingressLinksIn.length; i++) {
				if (i > 0) {
					sb.append(",");
				}
				sb.append("\n{\n");
				IngressLink ingressLink = this.ingressLinksIn[i];
				sb.append(ingressLink == null ? "<null>" : ingressLink.toString());
				sb.append("\n}");
			}
		}
		sb.append("\n}");
		return sb.toString();
	}
}
