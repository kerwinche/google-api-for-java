package mra.google.ingress;

public abstract class IngressFlipCards extends IngressStorableInventory {

	IngressFlipCards(String guid, IngressAgent owner, String name) {
		super(guid, owner, 100, name, "Very Rare");
	}
}
