package mra.google.ingress;

public abstract class IngressObject {

	private final String guid;

	IngressObject(String guid) {
		this.guid = guid;
	}

	public String getGuid() {
		return this.guid;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("GUID = ");
		sb.append(this.getGuid());
		return sb.toString();
	}
}
