package mra.google.ingress;

public class IngressTurret extends IngressMods {

	private final int hitBonus;
	private final int attackFrequency;

	IngressTurret(String guid, IngressAgent owner, int removalStickiness) {
		super(guid, owner, 80, "Turret", "Rare", 800, removalStickiness);
		this.hitBonus = 300;
		this.attackFrequency = 2;
	}

	public int getHitBonus() {
		return this.hitBonus;
	}

	public int getAttackFrequency() {
		return this.attackFrequency;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("\nHit Bonus = ");
		sb.append(this.getHitBonus());
		sb.append("\nAttack Frequency = ");
		sb.append(this.getAttackFrequency());
		return sb.toString();
	}
}
