package mra.google.ingress;

public class IngressScore {

	private final String regionName;
	private final long enlightenedScore;
	private final long resistanceScore;

	IngressScore(String regionName, long enlightenedScore, long resistanceScore) {
		this.regionName = regionName;
		this.enlightenedScore = enlightenedScore;
		this.resistanceScore = resistanceScore;
	}

	public String getRegionName() {
		return this.regionName;
	}

	public long getEnlightenedScore() {
		return this.enlightenedScore;
	}

	public long getResistanceScore() {
		return this.resistanceScore;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Region Name = ");
		sb.append(((this.getRegionName() == null) ? "<null>" : this.getRegionName()));
		sb.append("\nEnlightened Score = ");
		sb.append(this.getEnlightenedScore());
		sb.append("\nResistance Score = ");
		sb.append(this.getResistanceScore());
		return sb.toString();
	}
}
