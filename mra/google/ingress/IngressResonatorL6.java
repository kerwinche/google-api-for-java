package mra.google.ingress;

public class IngressResonatorL6 extends IngressResonators {

	IngressResonatorL6(String guid, IngressAgent owner) {
		this(guid, owner, -1, -1);
	}

	IngressResonatorL6(String guid, IngressAgent owner, int remainedEnergy, int distance) {
		super(guid, owner, 6, 4000, remainedEnergy, distance);
	}
}
