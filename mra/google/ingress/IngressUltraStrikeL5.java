package mra.google.ingress;

public class IngressUltraStrikeL5 extends IngressUltraStrikes {

	IngressUltraStrikeL5(String guid, IngressAgent owner) {
		super(guid, owner, 5, 1200, 21, 250);
	}
}
