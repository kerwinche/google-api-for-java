package mra.google.calendar;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;
import mra.google.Google;
import mra.google.GoogleAuthorization;
import mra.http.HttpRequest;
import mra.json.JsonArray;
import mra.json.JsonObject;
import mra.json.JsonParser;
import mra.json.JsonString;
import mra.json.JsonValue;

public class Calendar {

	static Map<String, String> createCalendarGoogleAuthorizationHeaders(String contentType, GoogleAuthorization googleAuthorization) {
		Map<String, String> requestHeaders = new LinkedHashMap<>();
		requestHeaders.put("Content-Type", contentType + "; charset=" + HttpRequest.CHARSET_UTF8);
		requestHeaders.put("GData-Version", "3");
		if (googleAuthorization != null) {
			requestHeaders.put("Authorization", googleAuthorization.getToken());
		}
		return requestHeaders;
	}

	private GoogleAuthorization googleAuthorization;

	GoogleAuthorization getGoogleAuthorization() {
		return this.googleAuthorization;
	}

	public Calendar(String id, GoogleAuthorization googleAuthorization) throws IOException {
		this.googleAuthorization = googleAuthorization;
		URL url = new URL("https://www.googleapis.com/calendar/v3/calendars/" + id + "/events");
		HttpRequest httpRequest = new HttpRequest(url);
		httpRequest.setMethod(HttpRequest.METHOD_GET);
		httpRequest.setRequestHeaders(Calendar.createCalendarGoogleAuthorizationHeaders(HttpRequest.CONTENT_TYPE_WWW_FORM, googleAuthorization));
		httpRequest.send();
		String responseString = httpRequest.getResponseString();
		JsonValue jsonValue = JsonParser.parse(responseString);
		JsonObject jsonObject = ((JsonObject) jsonValue);
		JsonValue jvItems = jsonObject.getValue("items");
		JsonArray jaItems = ((JsonArray) jvItems);
		Pattern pattern = Pattern.compile("^([0-9]{4})-([0-9]{2})-([0-9]{2})(.([0-9]{2}):([0-9]{2}):([0-9]{2})(.([0-9]{2}):([0-9]{2}))?)?");
		for (JsonValue jvItem : jaItems.getValue()) {
			JsonObject joItem = ((JsonObject) jvItem);
			String eid = ((JsonString) joItem.getValue("id")).getValue();
			String title = null;
			String location = null;
			Date start = null;
			Date end = null;
			try {
				title = ((JsonString) joItem.getValue("summary")).getValue();
			} catch (Exception ex) {
			}
			try {
				location = ((JsonString) joItem.getValue("location")).getValue();
			} catch (Exception ex) {
			}
			try {
				JsonValue jvStart = joItem.getValue("start");
				JsonObject joStart = ((JsonObject) jvStart);
				String dateTime = ((JsonString) joStart.getValue("dateTime")).getValue();
				start = Google.datetimeStringToDate(dateTime);
			} catch (Exception ex) {
			}
			try {
				JsonValue jvEnd = joItem.getValue("end");
				JsonObject joEnd = ((JsonObject) jvEnd);
				String dateTime = ((JsonString) joEnd.getValue("dateTime")).getValue();
				end = Google.datetimeStringToDate(dateTime);
			} catch (Exception ex) {
			}
			Event event = new Event(eid, title, location, start, end);
			System.out.println(event.toString());
		}
	}
}
