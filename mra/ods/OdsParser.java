package mra.ods;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Stack;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import mra.xml.XmlException;
import mra.xml.XmlParser;
import mra.xml.XmlString;
import mra.xml.XmlTag;

public class OdsParser {

	public static Map<String, String[][]> parse(File odsFile) throws IOException, XmlException {
		Map<String, String[][]> tables = new LinkedHashMap<>();
		Stack<Stack<String>> rows = new Stack<>();
		ZipInputStream zis = new ZipInputStream(new FileInputStream(odsFile));
		for (ZipEntry zipEntry; (zipEntry = zis.getNextEntry()) != null;) {
			if (zipEntry.getName().equals("content.xml")) {
				StringBuilder sb = new StringBuilder();
				byte[] buffer = new byte[1024];
				for (int length; (length = zis.read(buffer)) > -1;) {
					sb.append(new String(buffer, 0, length));
				}
				XmlTag xmlTag = XmlParser.parse(sb.toString());
				XmlTag[] xmlTables = xmlTag.getXmlTags("office:body")[0].getXmlTags("office:spreadsheet")[0].getXmlTags("table:table");
				for (XmlTag xmlTable : xmlTables) {
					String sheetName = xmlTable.getXmlAttribute("table:name");
					XmlTag[] xmlRows = xmlTable.getXmlTags("table:table-row");
					for (XmlTag xmlRow : xmlRows) {
						Stack<String> cells = new Stack<>();
						XmlTag[] xmlCells = xmlRow.getXmlTags("table:table-cell");
						for (XmlTag xmlCell : xmlCells) {
							XmlTag[] xmlText = xmlCell.getXmlTags("text:p");
							if (xmlText.length == 1) {
								String s = ((XmlString) xmlText[0].getXmlObject(0)).getString();
								cells.push(s);
							} else {
								String colspan = (xmlCell.getXmlAttribute("table:number-columns-repeated"));
								int cols = ((colspan == null) ? 1 : Integer.parseInt(colspan));
								for (int i = 0; i < cols; i++) {
									cells.push(null);
								}
							}
						}
						rows.push(cells);
					}
					int max = 0;
					for (Stack<String> row : rows) {
						max = Math.max(max, row.size());
					}
					String[][] stringss = new String[rows.size()][max];
					for (int i = 0; i < rows.size(); i++) {
						Stack<String> cells = rows.elementAt(i);
						for (int j = 0; j < cells.size(); j++) {
							stringss[i][j] = cells.elementAt(j);
						}
					}
					tables.put(sheetName, stringss);
				}
			}
		}
		zis.closeEntry();
		zis.close();
		return tables;
	}
}
