package mra.dimension;

public class Matrix<T> {

	private T[][] data;

	public Matrix(T[][] data) {
		int rows = 0;
		int columns = 0;
		for (T[] rowData : data) {
			rows++;
			columns = Math.max(columns, rowData.length);
		}
		this.resize(rows, columns, false);
		for (int row = 0; row < data.length; row++) {
			for (int column = 0; column < data[row].length; column++) {
				this.set(row, column, data[row][column]);
			}
		}
	}

	public Matrix(int rows, int columns) {
		this((T[][]) new Object[rows][columns]);
	}

	public Matrix() {
		this(0, 0);
	}

	public int getRowCount() {
		return this.data.length;
	}

	public int getColumnCount() {
		int columns = 0;
		for (T[] rowData : this.data) {
			columns = Math.max(columns, rowData.length);
		}
		return columns;
	}

	public void set(int row, int column, T data) {
		int rows = row + 1;
		int columns = column + 1;
		if (rows > this.getRowCount() || columns > this.getColumnCount()) {
			rows = Math.max(rows, this.getRowCount());
			columns = Math.max(columns, this.getColumnCount());
			this.resize(rows, columns, true);
		}
		this.data[row][column] = data;
	}

	public T get(int row, int column) {
		if (row >= this.getRowCount() || column >= this.getColumnCount()) {
			return null;
		} else {
			return this.data[row][column];
		}
	}

	public void resize(int rows, int columns, boolean keepData) {
		if (rows < 0) {
			rows = this.getRowCount();
		}
		if (columns < 0) {
			columns = this.getColumnCount();
		}
		T[][] data = (T[][]) new Object[rows][columns];
		if (keepData) {
			rows = Math.min(rows, this.getRowCount());
			columns = Math.min(columns, this.getColumnCount());
			for (int row = 0; row < rows; row++) {
				for (int column = 0; column < columns; column++) {
					data[row][column] = this.get(row, column);
				}
			}
		}
		this.data = data;
	}

	public void copy(int row1, int column1, int row2, int column2) {
		int rows = Math.max(row1, row2) + 1;
		int columns = Math.max(column1, column2) + 1;
		if (rows > this.getRowCount() || columns > this.getColumnCount()) {
			rows = Math.max(rows, this.getRowCount());
			columns = Math.max(columns, this.getColumnCount());
			this.resize(rows, columns, true);
		}
		this.set(row2, column2, this.get(row1, column1));
	}

	public void copy(int row1, int column1, int rows, int columns, int row2, int column2) {
		for (int row = row1 + rows - 1, r = rows - 1; row >= row1; row--, r--) {
			for (int column = column1 + columns - 1, c = columns - 1; column >= column1; column--, c--) {
				this.copy(row, column, row2 + r, column2 + c);
			}
		}
	}

	public void move(int row1, int column1, int row2, int column2) {
		int rows = Math.max(row1, row2) + 1;
		int columns = Math.max(column1, column2) + 1;
		if (rows > this.getRowCount() || columns > this.getColumnCount()) {
			rows = Math.max(rows, this.getRowCount());
			columns = Math.max(columns, this.getColumnCount());
			this.resize(rows, columns, true);
		}
		this.copy(row1, column1, row2, column2);
		this.set(row1, column1, null);
	}

	public void move(int row1, int column1, int rows, int columns, int row2, int column2) {
		for (int row = row1 + rows - 1, r = rows - 1; row >= row1; row--, r--) {
			for (int column = column1 + columns - 1, c = columns - 1; column >= column1; column--, c--) {
				this.move(row, column, row2 + r, column2 + c);
			}
		}
	}

	public void swap(int row1, int column1, int row2, int column2) {
		int rows = Math.max(row1, row2) + 1;
		int columns = Math.max(column1, column2) + 1;
		if (rows > this.getRowCount() || columns > this.getColumnCount()) {
			rows = Math.max(rows, this.getRowCount());
			columns = Math.max(columns, this.getColumnCount());
			this.resize(rows, columns, true);
		}
		T data = this.get(row2, column2);
		this.copy(row1, column1, row2, column2);
		this.set(row1, column1, data);
	}

	public void swap(int row1, int column1, int rows, int columns, int row2, int column2) {
		for (int row = row1 + rows - 1, r = rows - 1; row >= row1; row--, r--) {
			for (int column = column1 + columns - 1, c = columns - 1; column >= column1; column--, c--) {
				this.swap(row, column, row2 + r, column2 + c);
			}
		}
	}

	public void horizontalFlip(int row, int column, int rows, int columns) {
		for (int c = column; c < columns / 2; c++) {
			this.swap(row, c, rows, 1, row, columns - c - 1);
		}
	}

	public void horizontalFlip() {
		this.horizontalFlip(0, 0, this.getRowCount(), this.getColumnCount());
	}

	public void verticalFlip(int row, int column, int rows, int columns) {
		for (int r = row; r < rows / 2; r++) {
			this.swap(r, column, 1, columns, rows - r - 1, column);
		}
	}

	public void verticalFlip() {
		this.verticalFlip(0, 0, this.getRowCount(), this.getColumnCount());
	}

	public void transpose(int row1, int column1, int rows, int columns, int row2, int column2) {
		int newRows = Math.max(row1, column2) + 1;
		int newColumns = Math.max(column1, row2) + 1;
		if (newRows > this.getRowCount() || newColumns > this.getColumnCount()) {
			newRows = Math.max(newRows, this.getRowCount());
			newColumns = Math.max(newColumns, this.getColumnCount());
			this.resize(newRows, newColumns, true);
		}
		for (int row = row1 + rows - 1, c = rows - 1; row >= row1; row--, c--) {
			for (int column = column1 + columns - 1, r = columns - 1; column >= column1; column--, r--) {
				this.copy(row, column, row2 + r, column2 + c);
			}
		}
	}

	public void transpose() {
		int rows = this.getRowCount();
		int columns = this.getColumnCount();
		int size = Math.max(rows, columns);
		T[][] data = (T[][]) new Object[size][size];
		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				data[column][row] = this.get(row, column);
			}
		}
		this.data = data;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int rows = this.getRowCount();
		int columns = this.getColumnCount();
		for (int row = 0; row < rows; row++) {
			if (row > 0) {
				sb.append("\n");
			}
			for (int column = 0; column < columns; column++) {
				if (column > 0) {
					sb.append(",");
				}
				sb.append(this.get(row, column));
			}
		}
		return sb.toString();
	}
}
