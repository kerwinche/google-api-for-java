package mra.ini;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IniParser {

	private final static Pattern SECTION_PATTERN = Pattern.compile("^\\s*\\[([^\\[\\]]+)\\]\\s*$");
	private final static Pattern PAIR_PATTERN = Pattern.compile("^([^=]+)=(.*)$");

	public static IniObject parse(File iniFile, String commentCharString) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(iniFile));
		StringBuilder sb = new StringBuilder();
		for (String line; (line = br.readLine()) != null;) {
			sb.append("\n");
			sb.append(line);
		}
		sb.delete(0, 1);
		return IniParser.parse(sb.toString(), commentCharString);
	}

	public static IniObject parse(File iniFile) throws IOException {
		return IniParser.parse(iniFile, "");
	}

	public static IniObject parse(String ini, String commentCharString) {
		char[] commentChars = commentCharString.toCharArray();
		IniObject iniObject = new IniObject();
		String section = null;
		int line = 0;
		for (String row : ini.split("\n")) {
			line++;
			StringBuilder tempString = new StringBuilder();
			row:
			for (char c : row.toCharArray()) {
				for (char cc : commentChars) {
					if (c == cc) {
						break row;
					}
				}
				tempString.append(c);
			}
			row = tempString.toString();
			Matcher sectionMatcher = IniParser.SECTION_PATTERN.matcher(row);
			Matcher pairMatcher = IniParser.PAIR_PATTERN.matcher(row);
			if (row.trim().length() > 0) {
				if (sectionMatcher.matches()) {
					section = sectionMatcher.group(1);
				} else if (pairMatcher.matches()) {
					String key = pairMatcher.group(1);
					String value = pairMatcher.group(2);
					iniObject.setIniAttribute(section, key.trim(), value);
				} else {
					throw new IniException(String.format("Parse Ini Error @ line " + line));
				}
			}
		}
		return iniObject;
	}

	public static IniObject parse(String ini) {
		return IniParser.parse(ini, "");
	}

	private IniParser() {
	}
}
