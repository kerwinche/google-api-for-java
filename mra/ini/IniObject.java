package mra.ini;

import java.util.LinkedHashMap;
import java.util.Map;

public class IniObject {

	private final Map<String, Map<String, String>> iniAttributes;

	public IniObject(Map<String, Map<String, String>> iniAttributes) {
		this.iniAttributes = iniAttributes;
	}

	public IniObject() {
		this(new LinkedHashMap<String, Map<String, String>>());
	}

	public void setIniAttribute(String section, String key, String value) {
		if (!this.iniAttributes.containsKey(section)) {
			this.iniAttributes.put(section, new LinkedHashMap<String, String>());
		}
		this.iniAttributes.get(section).put(key, value);
	}

	public String getIniAttribute(String section, String key) {
		return this.iniAttributes.get(section).get(key);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (String section : this.iniAttributes.keySet()) {
			Map<String, String> iniAttributes = this.iniAttributes.get(section);
			if (section != null) {
				sb.append("\n");
				sb.append("[");
				sb.append(section);
				sb.append("]");
			}
			for (String key : iniAttributes.keySet()) {
				String value = iniAttributes.get(key);
				sb.append("\n");
				sb.append(key);
				sb.append("=");
				sb.append(value);
			}
		}
		sb.delete(0, 1);
		return sb.toString();
	}
}
