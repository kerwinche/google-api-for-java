package mra.json;

public class JsonFalse extends JsonBoolean {

	public static JsonFalse parse(String json) throws JsonException {
		JsonBoolean jsonBoolean = JsonBoolean.parse(json);
		if (jsonBoolean instanceof JsonFalse) {
			return (JsonFalse) jsonBoolean;
		} else {
			throw new JsonException("not a JSON false");
		}
	}

	public JsonFalse() {
		super(false);
	}
}
