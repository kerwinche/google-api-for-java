package mra.json;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Stack;
import mra.xml.XmlObject;
import mra.xml.XmlTag;

public class JsonArray extends JsonValue<JsonValue[]> {

	public static JsonArray parse(String json) throws JsonException {
		Stack<String> strings = JsonParser.extract(json);
		String string = strings.remove(0);
		if (string.equals("[")) {
			return JsonArray.constructJsonArray(strings, new JsonArray());
		} else {
			throw new JsonException("JSON array must start with \"[]\" pair");
		}
	}

	static JsonArray constructJsonArray(Stack<String> strings, JsonArray jsonArray) throws JsonException {
		if (strings.size() > 0) {
			int state = JsonParser.JSON_STATE_OPEN;
			while (strings.size() > 0) {
				String string = strings.remove(0);
				if (string.equals("[")) {
					if (state == JsonParser.JSON_STATE_OPEN || state == JsonParser.JSON_STATE_COMMA) {
						jsonArray.addValue(JsonArray.constructJsonArray(strings, new JsonArray()));
						state = JsonParser.JSON_STATE_VALUE;
					} else {
						throw new JsonException("JSON value must after \"[\" or \",\" in JSON array");
					}
				} else if (string.equals("]")) {
					if (state == JsonParser.JSON_STATE_OPEN || state == JsonParser.JSON_STATE_VALUE) {
						break;
					} else {
						throw new JsonException("JSON array must close after \"{\" or JSON value in JSON array");
					}
				} else if (string.equals("{")) {
					if (state == JsonParser.JSON_STATE_OPEN || state == JsonParser.JSON_STATE_COMMA) {
						jsonArray.addValue(JsonObject.constructJsonObject(strings, new JsonObject()));
						state = JsonParser.JSON_STATE_VALUE;
					} else {
						throw new JsonException("JSON value must after \"[\" or \",\" in JSON array");
					}
				} else if (string.equals(",")) {
					if (state == JsonParser.JSON_STATE_VALUE) {
						state = JsonParser.JSON_STATE_COMMA;
					} else {
						throw new JsonException("\",\" must after JSON value in JSON object");
					}
				} else {
					if (state == JsonParser.JSON_STATE_OPEN || state == JsonParser.JSON_STATE_COMMA) {
						JsonValue jsonValue = JsonParser.createJsonOther(string);
						if (jsonValue == null) {
							throw new JsonException("JSON only accept JSON \"null\", \"true\", \"false\", integer, float or string");
						} else {
							jsonArray.addValue(jsonValue);
							state = JsonParser.JSON_STATE_VALUE;
						}
					} else {
						throw new JsonException("JSON value must after \"[\" or \",\" in JSON object");
					}
				}
			}
			return jsonArray;
		} else {
			return null;
		}
	}
	private ArrayList<JsonValue> values = new ArrayList<>();

	public JsonArray() {
		this(new JsonValue[0]);
	}

	public JsonArray(JsonValue[] value) {
		super(null);
		this.values.addAll(Arrays.asList(value));
	}

	public void addValue(JsonValue value) {
		this.values.add(value);
	}

	public int countJsonValues() {
		return this.values.size();
	}

	@Override
	public JsonValue[] getValue() {
		return this.values.toArray(new JsonValue[this.values.size()]);
	}

	public JsonValue getValue(int index) {
		return this.values.get(index);
	}

	@Override
	String toString(boolean formatted, int tabs, boolean isFirst) {
		StringBuilder sb = new StringBuilder();
		if (!isFirst && formatted) {
			sb.append("\n");
			for (int j = 0; j < tabs; j++) {
				sb.append(JsonString.CHAR_TAB);
			}
		}
		sb.append('[');
		JsonValue[] values = this.getValue();
		if (values.length > 0) {
			for (int i = 0; i < values.length; i++) {
				if (i > 0) {
					sb.append(',');
				}
				sb.append(values[i].toString(formatted, tabs + 1, false));
			}
			if (formatted) {
				sb.append("\n");
				for (int j = 0; j < tabs; j++) {
					sb.append(JsonString.CHAR_TAB);
				}
			}
		}
		sb.append(']');
		return sb.toString();
	}

	@Override
	public XmlObject toXmlObject() {
		return this.toXmlObject(null);
	}

	XmlObject toXmlObject(String root) {
		if (root == null) {
			root = "root-" + new SimpleDateFormat("YYYY-MM-dd-HH-mm-ss-SSS").format(new Date());
		}
		StringBuilder sb = new StringBuilder();
		XmlTag xmlTag = new XmlTag(root + "-s");
		for (JsonValue jsonValue : this.getValue()) {
			if (jsonValue instanceof JsonObject) {
				xmlTag.addXmlObject(((JsonObject) jsonValue).toXmlObject(root));
			} else {
				xmlTag.addXmlObject(jsonValue.toXmlObject());
			}
		}
		return xmlTag;
	}
}
