package mra.json;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Stack;

public class JsonParser {

	static final String TAB = "\t";
	static final int JSON_STATE_OPEN = 0;
	static final int JSON_STATE_KEY = JsonParser.JSON_STATE_OPEN + 1;
	static final int JSON_STATE_COLON = JsonParser.JSON_STATE_KEY + 1;
	static final int JSON_STATE_VALUE = JsonParser.JSON_STATE_COLON + 1;
	static final int JSON_STATE_COMMA = JsonParser.JSON_STATE_VALUE + 1;

	public static JsonValue parse(File jsonFile) throws IOException, JsonException {
		BufferedReader br = new BufferedReader(new FileReader(jsonFile));
		StringBuilder sb = new StringBuilder();
		for (String line; (line = br.readLine()) != null;) {
			sb.append("\n");
			sb.append(line);
		}
		sb.delete(0, 1);
		return JsonParser.parse(sb.toString());
	}

	static Stack<String> extract(String json) throws JsonException {
		json = json.trim();
		int line = 1;
		int column = 0;
		Stack<Character> bracketPairs = new Stack<>();
		Stack<String> strings = new Stack<>();
		StringBuilder tempString = new StringBuilder();
		boolean openEscape = false;
		boolean openString = false;
		for (char c : json.toCharArray()) {
			column++;
			tempString.append(c);
			switch (c) {
				case '\n': {
					line++;
					column = 0;
					openEscape = false;
				}
				break;
				case '{': {
					if (openString) {
						openEscape = false;
					} else {
						bracketPairs.push(c);
						strings.push(tempString.toString().trim());
						tempString.delete(0, tempString.length());
					}
				}
				break;
				case '}': {
					if (openString) {
						openEscape = false;
					} else {
						if (bracketPairs.peek() == '{') {
							bracketPairs.pop();
							String string = tempString.toString().trim();
							int index = string.lastIndexOf('}');
							String previous = string.substring(0, index).trim();
							if (previous.length() > 0) {
								strings.push(previous);
							}
							strings.push(string.substring(index).trim());
							tempString.delete(0, tempString.length());
						} else {
							throw new JsonException("JSON object pair not match @ line " + line + " column " + column);
						}
					}
				}
				break;
				case '[': {
					if (openString) {
						openEscape = false;
					} else {
						bracketPairs.push(c);
						strings.push(tempString.toString().trim());
						tempString.delete(0, tempString.length());
					}
				}
				break;
				case ']': {
					if (openString) {
						openEscape = false;
					} else {
						if (bracketPairs.peek() == '[') {
							bracketPairs.pop();
							String string = tempString.toString().trim();
							int index = string.lastIndexOf(']');
							String previous = string.substring(0, index).trim();
							if (previous.length() > 0) {
								strings.push(previous);
							}
							strings.push(string.substring(index).trim());
							tempString.delete(0, tempString.length());
						} else {
							throw new JsonException("JSON array pair not match @ line " + line + " column " + column);
						}
					}
				}
				break;
				case ':': {
					if (openString) {
						openEscape = false;
					} else {
						strings.push(tempString.toString().trim());
						tempString.delete(0, tempString.length());
					}
				}
				break;
				case ',': {
					if (openString) {
						openEscape = false;
					} else {
						String string = tempString.toString().trim();
						int index = string.lastIndexOf(',');
						String previous = string.substring(0, index).trim();
						if (previous.length() > 0) {
							strings.push(previous);
						}
						strings.push(string.substring(index).trim());
						tempString.delete(0, tempString.length());
					}
				}
				break;
				case '"': {
					if (openString) {
						if (openEscape) {
							openEscape = false;
						} else {
							strings.push(tempString.toString().trim());
							tempString.delete(0, tempString.length());
							openString = false;
						}
					} else {
						openString = true;
					}
				}
				break;
				case '\\': {
					if (openString) {
						openEscape = !openEscape;
					}
				}
				break;
				default: {
					openEscape = false;
				}
			}
		}
		if (bracketPairs.empty()) {
			return strings;
		} else {
			throw new JsonException("JSON object or array pair not match @ line " + line + " column " + column);
		}
	}

	public static JsonValue parse(String json) throws JsonException {
		Stack<String> strings = JsonParser.extract(json);
		if (strings.empty()) {
			throw new JsonException("No data can be extracted");
		} else {
			String string = strings.remove(0);
			if (string.equals("{")) {
				return JsonObject.constructJsonObject(strings, new JsonObject());
			} else if (string.equals("[")) {
				return JsonArray.constructJsonArray(strings, new JsonArray());
			} else {
				throw new JsonException("JSON must start with \"[]\" or \"{}\" pair");
			}
		}
	}

	static JsonValue createJsonOther(String string) {
		JsonValue jsonValue = null;
		if (jsonValue == null) {
			try {
				jsonValue = JsonString.parse(string);
			} catch (JsonException ex) {
			}
		}
		if (jsonValue == null) {
			try {
				jsonValue = JsonNumber.parse(string);
			} catch (JsonException ex) {
			}
		}
		if (jsonValue == null) {
			try {
				jsonValue = JsonFalse.parse(string);
			} catch (JsonException ex) {
			}
		}
		if (jsonValue == null) {
			try {
				jsonValue = JsonTrue.parse(string);
			} catch (JsonException ex) {
			}
		}
		if (jsonValue == null) {
			try {
				jsonValue = JsonNull.parse(string);
			} catch (JsonException ex) {
			}
		}
		return jsonValue;
	}

	private JsonParser() {
	}
}
