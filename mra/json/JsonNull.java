package mra.json;

public class JsonNull extends JsonBoolean {

	public static JsonNull parse(String json) throws JsonException {
		if (json.trim().equals("null")) {
			return new JsonNull();
		} else {
			throw new JsonException("not a JSON null");
		}
	}

	public JsonNull() {
		super(null);
	}
}
