package mra.json;

import mra.xml.XmlObject;

public abstract class JsonValue<T> {

	private final T value;

	public JsonValue(T value) {
		this.value = value;
	}

	public T getValue() {
		return this.value;
	}

	@Override
	public String toString() {
		return this.toString(false);
	}

	public String toString(boolean formatted) {
		return this.toString(formatted, 0, true);
	}

	abstract String toString(boolean formatted, int tabs, boolean isFirst);

	public abstract XmlObject toXmlObject();
}
