package mra.json;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Stack;
import mra.xml.XmlObject;
import mra.xml.XmlTag;

public class JsonObject extends JsonValue<Map<String, JsonValue>> {

	public static JsonObject parse(String json) throws JsonException {
		Stack<String> strings = JsonParser.extract(json);
		String string = strings.remove(0);
		if (string.equals("{")) {
			return JsonObject.constructJsonObject(strings, new JsonObject());
		} else {
			throw new JsonException("JSON object must start with \"{}\" pair");
		}
	}

	static JsonObject constructJsonObject(Stack<String> strings, JsonObject jsonObject) throws JsonException {
		if (strings.size() > 0) {
			String key = null;
			int state = JsonParser.JSON_STATE_OPEN;
			while (strings.size() > 0) {
				String string = strings.remove(0);
				if (string.equals("{")) {
					if (state == JsonParser.JSON_STATE_COLON) {
						jsonObject.setValue(key, JsonObject.constructJsonObject(strings, new JsonObject()));
						state = JsonParser.JSON_STATE_VALUE;
					} else {
						throw new JsonException("JSON value must after \":\" in JSON object");
					}
				} else if (string.equals("}")) {
					if (state == JsonParser.JSON_STATE_OPEN || state == JsonParser.JSON_STATE_VALUE) {
						break;
					} else {
						throw new JsonException("JSON object must close after \"{\" or JSON value in JSON object");
					}
				} else if (string.equals("[")) {
					if (state == JsonParser.JSON_STATE_COLON) {
						jsonObject.setValue(key, JsonArray.constructJsonArray(strings, new JsonArray()));
						state = JsonParser.JSON_STATE_VALUE;
					} else {
						throw new JsonException("JSON value must after \":\" in JSON object");
					}
				} else if (string.equals(":")) {
					if (state == JsonParser.JSON_STATE_KEY) {
						state = JsonParser.JSON_STATE_COLON;
					} else {
						throw new JsonException("\":\" must after JSON object attribute key in JSON object");
					}
				} else if (string.equals(",")) {
					if (state == JsonParser.JSON_STATE_VALUE) {
						state = JsonParser.JSON_STATE_COMMA;
					} else {
						throw new JsonException("\",\" must after JSON value in JSON object");
					}
				} else {
					if (state == JsonParser.JSON_STATE_OPEN || state == JsonParser.JSON_STATE_COMMA) {
						try {
							key = JsonString.parse(string).getValue();
							state = JsonParser.JSON_STATE_KEY;
						} catch (JsonException ex) {
							throw new JsonException("JSON object attribute key must after \"{\" or \",\" in JSON object");
						}
					} else if (state == JsonParser.JSON_STATE_COLON) {
						JsonValue jsonValue = JsonParser.createJsonOther(string);
						if (jsonValue == null) {
							throw new JsonException("JSON only accept JSON \"null\", \"true\", \"false\", integer, float or string");
						} else {
							jsonObject.setValue(key, jsonValue);
							state = JsonParser.JSON_STATE_VALUE;
						}
					} else {
						throw new JsonException("JSON value must after \":\" in JSON object");
					}
				}
			}
			return jsonObject;
		} else {
			return null;
		}
	}
	private final Map<String, JsonValue> jsonAttributes;

	public JsonObject() {
		this(new LinkedHashMap<String, JsonValue>());
	}

	public JsonObject(Map<String, JsonValue> jsonAttributes) {
		super(null);
		this.jsonAttributes = jsonAttributes;
	}

	public void setValue(String key, JsonValue value) {
		this.jsonAttributes.put(key, value);
	}

	@Override
	public Map<String, JsonValue> getValue() {
		return this.jsonAttributes;
	}

	public JsonValue getValue(String key) {
		return this.jsonAttributes.get(key);
	}

	@Override
	String toString(boolean formatted, int tabs, boolean isFirst) {
		StringBuilder sb = new StringBuilder();
		if (!isFirst && formatted) {
			sb.append("\n");
			for (int j = 0; j < tabs; j++) {
				sb.append(JsonString.CHAR_TAB);
			}
		}
		sb.append('{');
		if (this.jsonAttributes.size() > 0) {
			int i = 0;
			for (String key : this.jsonAttributes.keySet()) {
				if (i > 0) {
					sb.append(',');
				}
				JsonValue value = this.jsonAttributes.get(key);
				if (formatted) {
					sb.append("\n");
					for (int j = 0; j < tabs + 1; j++) {
						sb.append(JsonString.CHAR_TAB);
					}
				}
				sb.append('"');
				sb.append(key);
				sb.append("\":");
				if (value instanceof JsonObject || value instanceof JsonArray) {
					sb.append(value.toString(formatted, tabs + 1, false));
				} else {
					sb.append(value.toString(formatted, tabs + 1, true));
				}
				i++;
			}
			if (formatted) {
				sb.append("\n");
				for (int j = 0; j < tabs; j++) {
					sb.append(JsonString.CHAR_TAB);
				}
			}
		}
		sb.append('}');
		return sb.toString();
	}

	@Override
	public XmlObject toXmlObject() {
		return this.toXmlObject(null);
	}

	XmlObject toXmlObject(String root) {
		if (root == null) {
			root = "root-" + new SimpleDateFormat("YYYY-MM-dd-HH-mm-ss-SSS").format(new Date());
		}
		XmlTag xmlTag = new XmlTag(root);
		for (String key : this.jsonAttributes.keySet()) {
			JsonValue jsonValue = this.jsonAttributes.get(key);
			key = "_" + key;
			if (jsonValue instanceof JsonObject) {
				xmlTag.addXmlObject(((JsonObject) jsonValue).toXmlObject(key));
			} else if (jsonValue instanceof JsonArray) {
				xmlTag.addXmlObject(((JsonArray) jsonValue).toXmlObject(key));
			} else {
				xmlTag.addXmlObject(new XmlTag(key, new XmlObject[]{jsonValue.toXmlObject()}));
			}
		}
		return xmlTag;
	}
}
