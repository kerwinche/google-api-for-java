package mra.json;

import mra.xml.XmlObject;
import mra.xml.XmlString;
import mra.xml.XmlTag;

public class JsonString extends JsonValue<String> {

	static final char CHAR_NULL = '\0';
	static final char CHAR_BELL = '\7';
	static final char CHAR_BACKSPACE = '\b';
	static final char CHAR_LINE_FEED = '\n';
	static final char CHAR_TAB = '\t';
	static final char CHAR_VERTICAL_TAB = '\11';
	static final char CHAR_FORM_FEED = '\f';
	static final char CHAR_CARRIAGE_RETURN = '\r';
	static final char CHAR_ESCAPE = '\27';
	static final char CHAR_DOUBLE_QUOTATION_MARK = '"';

	public static JsonString parse(String json) throws JsonException {
		json = json.trim();
		if (json.length() < 2 || json.charAt(0) != '"' || json.charAt(json.length() - 1) != '"') {
			throw new JsonException("JSON string must open and close with double quotation mark");
		}
		json = json.substring(1, json.length() - 1);
		if (json.contains(Character.toString(JsonString.CHAR_NULL))
			|| json.contains(Character.toString(JsonString.CHAR_BELL))
			|| json.contains(Character.toString(JsonString.CHAR_BACKSPACE))
			|| json.contains(Character.toString(JsonString.CHAR_LINE_FEED))
			|| json.contains(Character.toString(JsonString.CHAR_VERTICAL_TAB))
			|| json.contains(Character.toString(JsonString.CHAR_FORM_FEED))
			|| json.contains(Character.toString(JsonString.CHAR_CARRIAGE_RETURN))
			|| json.contains(Character.toString(JsonString.CHAR_ESCAPE))) {
			throw new JsonException("JSON string cannot contain control character");
		}
		StringBuilder tempString = new StringBuilder();
		boolean openEscape = false;
		int length = json.length();
		for (int i = 0; i < length; i++) {
			char c = json.charAt(i);
			switch (c) {
				case 'b': {
					if (openEscape) {
						int tempLength = tempString.length();
						if (tempLength > 0) {
							tempString.delete(tempLength - 1, tempLength);
						} else {
							throw new JsonException("no more characters cannot be deleted");
						}
					} else {
						tempString.append(c);
					}
					openEscape = false;
				}
				break;
				case 'f': {
					if (openEscape) {
						tempString.append('\f');
					} else {
						tempString.append(c);
					}
					openEscape = false;
				}
				break;
				case 'n': {
					if (openEscape) {
						tempString.append('\n');
					} else {
						tempString.append(c);
					}
					openEscape = false;
				}
				break;
				case 'r': {
					if (openEscape) {
						tempString.append('\r');
					} else {
						tempString.append(c);
					}
					openEscape = false;
				}
				break;
				case 't': {
					if (openEscape) {
						tempString.append('\t');
					} else {
						tempString.append(c);
					}
					openEscape = false;
				}
				break;
				case 'u': {
					if (openEscape) {
						try {
							String hexString = json.substring(i + 1, i + 5);
							tempString.append((char) Integer.parseInt(hexString, 16));
							i += 4;
						} catch (Exception ex) {
							throw new JsonException("\\u requires 4-digit hexadecimal");
						}
					} else {
						tempString.append(c);
					}
					openEscape = false;
				}
				break;
				case '"': {
					if (openEscape) {
						tempString.append(c);
						openEscape = false;
					} else {
						throw new JsonException("JSON string cannot contain a double quotation mark without escape before");
					}
				}
				break;
				case '\\': {
					if (openEscape) {
						tempString.append(c);
					}
					openEscape = !openEscape;
				}
				break;
				default: {
					tempString.append(c);
					openEscape = false;
				}
			}
		}
		json = tempString.toString();
		return new JsonString(json);
	}

	public JsonString(String value) {
		super(value);
	}

	public String toUnscapedString() {
		String string = this.getValue();
		string = string.replaceAll("\\\\", "\\\\\\\\");
		string = string.replaceAll(Character.toString(JsonString.CHAR_FORM_FEED), "\\\\f");
		string = string.replaceAll(Character.toString(JsonString.CHAR_LINE_FEED), "\\\\n");
		string = string.replaceAll(Character.toString(JsonString.CHAR_CARRIAGE_RETURN), "\\\\r");
		string = string.replaceAll(Character.toString(JsonString.CHAR_TAB), "\\\\t");
		string = string.replaceAll(Character.toString(JsonString.CHAR_DOUBLE_QUOTATION_MARK), "\\\\" + '"');
		return string;
	}

	@Override
	String toString(boolean formatted, int tabs, boolean isFirst) {
		StringBuilder sb = new StringBuilder();
		if (!isFirst && formatted) {
			sb.append("\n");
			for (int j = 0; j < tabs; j++) {
				sb.append(JsonString.CHAR_TAB);
			}
		}
		sb.append('"');
		sb.append(this.toUnscapedString());
		sb.append('"');
		return sb.toString();
	}

	@Override
	public XmlObject toXmlObject() {
		return new XmlTag("string", new XmlObject[]{new XmlString(this.getValue())});
	}
}
